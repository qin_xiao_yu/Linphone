package com.sxkeda.voip.event;

import org.linphone.core.LinphoneCall;

import com.sxkeda.voip.Business.CallInformation;

public interface Listener {
	public void updateRegationStatus(String string);
	public void updataCallStatus(LinphoneCall.State state);	
	public void updataLinphoneServiceStatus(boolean status);
	public void updataCallTime(String time);
	public void updataCallNumber(String number);
	public void updataCallInformation(CallInformation callInformation);
	
}
