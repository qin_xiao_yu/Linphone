package com.sxkeda.voip.event;

import java.util.ArrayList;

import org.linphone.core.LinphoneCall;

import com.sxkeda.voip.Business.CallInformation;

public class Operater {
	private ArrayList<Listener> mListener;
//	private Listener mListener;
	private static Operater instance;
	
	public static synchronized final Operater getInstance() {
		if (instance != null) return instance;		
		throw new RuntimeException("Operater should be created before accessed");
	}
	public Operater(){
		instance = this;
		mListener = new ArrayList<Listener>();
	}
	
	
	public void addListener(Listener listener)
	{
		this.mListener.add(listener);		
	}
	public void removeListener(Listener listener)
	{
		this.mListener.remove(listener);		
	}
	
	public synchronized void setCallStatus(LinphoneCall.State state)
	{
		if(mListener.size()>0){
			for(int i = 0 ; i<mListener.size() ; i++){
				mListener.get(i).updataCallStatus(state);
			}
		}
	}
	public synchronized void setRegationStatus(String string)
	{
		if(mListener.size()>0){
			for(int i = 0 ; i<mListener.size() ; i++){
				mListener.get(i).updateRegationStatus(string);
			}
		}
	}
	public synchronized void setLinphoneServiceStatus(boolean status){
		if(mListener.size()>0){
			for(int i = 0 ; i<mListener.size() ; i++){
				mListener.get(i).updataLinphoneServiceStatus(status);
			}
		}
	}
	public synchronized void setCallTime(String time){
		if(mListener.size()>0){
			for(int i = 0 ; i<mListener.size() ; i++){
				mListener.get(i).updataCallTime(time);
			}
		}
	}
	public synchronized void setCallNumber(String number){
		if(mListener.size()>0){
			for(int i = 0 ; i<mListener.size() ; i++){
				mListener.get(i).updataCallNumber(number);
			}
		}
	}
	public synchronized void setCallInformation(CallInformation callInformation){
		if(mListener.size()>0){
			for(int i = 0 ; i<mListener.size() ; i++){
				mListener.get(i).updataCallInformation(callInformation);
			}
		}
	}
}
