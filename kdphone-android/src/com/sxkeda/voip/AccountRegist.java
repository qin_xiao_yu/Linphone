package com.sxkeda.voip;

import org.linphone.LinphoneManager;
import org.linphone.LinphonePreferences;
import org.linphone.R;
import org.linphone.LinphonePreferences.AccountBuilder;
import org.linphone.core.LinphoneAddress;
import org.linphone.core.LinphoneCore;
import org.linphone.core.LinphoneCoreException;
import org.linphone.core.LinphoneCoreFactory;
import org.linphone.core.LinphoneProxyConfig;
import org.linphone.core.LinphoneAddress.TransportType;

import android.content.Context;

public class AccountRegist
{ 

	private String id,password,service,name;
	Context mContext;

	private Debug debug = new Debug(Debug.isKedaAccountRegist, "KedaAccountRegist");
 
	/**
	 * 向服务器注册linphone客户端
	 * @param context
	 * @param id
	 * @param password
	 * @param service
	 * @param name
	 *
	 * time 2016年7月18日上午10:13:39
	 */
	public AccountRegist(Context context,String id,String password,String service,String name)
	{
		mContext = context;
		this.id = id;
		this.password = password;
		this.service = service;
		this.name = name;
		debug.i("id = "+id);
		debug.i("password = "+password);
		debug.i("service = "+service);
		debug.i("name = "+name);
		LinphoneCore lc = LinphoneManager.getLc();
		LinphonePreferences mPrefs = LinphonePreferences.instance();
		int nbAccounts = LinphonePreferences.instance().getAccountCount();
		debug.i("nbAccounts = " + nbAccounts);
		
		try
        {
			if (nbAccounts == 1)
			{
				if(mPrefs.getAccountUserId(0) == null)
				{
					mPrefs.deleteAccount(0);
				}			
			}
			
        }
        catch (NullPointerException e)
        {
	        // TODO: handle exception
        	e.printStackTrace();
        }

		nbAccounts = LinphonePreferences.instance().getAccountCount();
		if (nbAccounts > 0)
		{
			debug.i("rechange account param");
			mPrefs.setAccountUsername(0, id);
			mPrefs.setAccountUserId(0, id);
			mPrefs.setAccountTransport(0, "pref_transport_tcp_key");
			mPrefs.setAccountPassword(0, /*"1234"*/password);
			mPrefs.setAccountDomain(0, /*"192.168.1.33"*/service);
			mPrefs.setAccountEnabled(0, true);			
			
			debug.i("getAccountUserId = "+mPrefs.getAccountUserId(0));
			debug.i("getAccountUsername = "+mPrefs.getAccountUsername(0));
			
			debug.i("change now account params");
		}
		else
		{
			saveCreatedAccount(id, "1234", "192.168.1.33");
		}
		
		
		nbAccounts = LinphonePreferences.instance().getAccountCount();
		debug.i("nbAccounts = " + nbAccounts);
		if (nbAccounts > 0)
		{
			debug.i("lc.refreshRegisters()");
			int accountIndex = 0;
			LinphonePreferences.instance().setDefaultAccount(accountIndex);
			// LinphoneCore lc = LinphoneManager.getLc();
			debug.i("accountPosition = " + 0);
			lc.setDefaultProxyConfig((LinphoneProxyConfig) LinphoneManager.getLc().getProxyConfigList()[0]);
			if (lc.isNetworkReachable())
			{
				lc.refreshRegisters();
			}
		}
	}

	public void saveCreatedAccount(String username, String password, String domain)
	{
		debug.i("saveCreatedAccount");
		// if (accountCreated)
		// return;
		// address;
		LinphonePreferences mPrefs = LinphonePreferences.instance();
		String identity = "sip:" + username + "@" + domain;
		try
		{
			LinphoneAddress address = LinphoneCoreFactory.instance().createLinphoneAddress(identity);
		}
		catch (LinphoneCoreException e)
		{
			e.printStackTrace();
		}
		boolean isMainAccountLinphoneDotOrg = domain.equals(mContext.getString(R.string.default_domain));
		boolean useLinphoneDotOrgCustomPorts = mContext.getResources().getBoolean(R.bool.use_linphone_server_ports);
		AccountBuilder builder = new AccountBuilder(LinphoneManager.getLc()).setUsername(username).setDomain(domain).setPassword(password);

		if (isMainAccountLinphoneDotOrg && useLinphoneDotOrgCustomPorts)
		{
			if (mContext.getResources().getBoolean(R.bool.disable_all_security_features_for_markets))
			{
				builder.setProxy(domain + ":5228").setTransport(TransportType.LinphoneTransportTcp);
			}
			else
			{
				builder.setProxy(domain + ":5223").setTransport(TransportType.LinphoneTransportTls);
			}

			builder
				.setExpires("604800")
				.setOutboundProxyEnabled(true)
				.setAvpfEnabled(true)
				.setAvpfRRInterval(3)
				.setQualityReportingCollector("sip:voip-metrics@sip.linphone.org")
				.setQualityReportingEnabled(true)
				.setQualityReportingInterval(180)
				.setRealm("sip.linphone.org");

			mPrefs.setStunServer(mContext.getString(R.string.default_stun));
			mPrefs.setIceEnabled(true);
		}
		// else {
		// String forcedProxy =
		// getResources().getString(R.string.setup_forced_proxy);
		// if (!TextUtils.isEmpty(forcedProxy)) {
		// builder.setProxy(forcedProxy)
		// .setOutboundProxyEnabled(true)
		// .setAvpfRRInterval(5);
		// }
		// }

		if (mContext.getResources().getBoolean(R.bool.enable_push_id))
		{
			String regId = mPrefs.getPushNotificationRegistrationID();
			String appId = mContext.getString(R.string.push_sender_id);
			if (regId != null && mPrefs.isPushNotificationEnabled())
			{
				String contactInfos = "app-id=" + appId + ";pn-type=google;pn-tok=" + regId;
				builder.setContactParameters(contactInfos);
			}
		}

		try
		{
			builder.saveNewAccount();
			// accountCreated = true;
		}
		catch (LinphoneCoreException e)
		{
			e.printStackTrace();
		}
	}

}
