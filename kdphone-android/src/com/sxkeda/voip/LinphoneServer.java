package com.sxkeda.voip;

import static android.content.Intent.ACTION_MAIN;

import org.linphone.LinphoneService;

import android.content.Context;
import android.content.Intent;

public class LinphoneServer
{
	private Context mContext;

	private Debug debug = new Debug(Debug.isKedaLinphoneServer, "KedaLinphoneServer");

	/**
	 * 初始化LinphoneServer
	 * 
	 * @author 秦晓宇
	 * @date 2016年6月25日 上午9:43:18
	 */
	public LinphoneServer(Context context)
	{
		debug.i("KedaLinphoneServer");
		mContext = context;

		if (LinphoneService.isReady())
		{
			debug.i("LinphoneService.isReady()");
		}
		else
		{
			// start linphone as background
			debug.i("start linphone as background");
			// final Intent intent = new Intent();
			// intent.setAction("org.linphone.LinphoneService");
			mContext.startService(new Intent(ACTION_MAIN).setClass(mContext, LinphoneService.class));
			// mContext.startService(intent);
		}
	}

}
