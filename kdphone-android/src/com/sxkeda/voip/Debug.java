package com.sxkeda.voip;

import android.util.Log;

public class Debug {
	
	
	
	public static final boolean isKedaMainActivity = true;
	public static final boolean isLinphoneService = true;
	public static final boolean isLinphoneManager = false;
	public static final boolean isLinphoneActivity = true;
	public static final boolean isSetupActivity = true;
	public static final boolean isLinphonePreferences = true;
	public static final boolean isPreferencesMigrator = true;
	public static final boolean isStatusFragment = true;
	
	public static final boolean isAccountsListAdapter = true;
	
	public static final boolean isVideoCallFragment = true;
	
	public static final boolean isCallVideoFragment = true;
	public static final boolean isCallActivity = true;
	public static final boolean isCallIncomingActivity = true;
	
	public static final boolean isKedaLinphoneServer = true;
	public static final boolean isKedaBusiness = true;
	public static final boolean isKedaAccountRegist = true;
	public static final boolean isKedaCallApi = true;
	public static final boolean isKedaUiMain = true;
	public static final boolean isVideoSize = true;
	
	
	public static final boolean isAndroidCameraConfigurationReader9 = true;
	public static final boolean isAndroidCameraConfiguration = true;
	public static final boolean isAACFilter = true;
	public static final boolean isLinphoneCoreImpl = true;
	
	
	
	
	
	
	
	
	
	
	private String tag;
	private boolean isDebug;

	
	public Debug(boolean isDebug,String tag)
	{
		this.isDebug = isDebug;
		this.tag = tag;		

	}

	public void i(String str)
	{
		if(isDebug == true && tag!=null)
		Log.i(tag, str);		
	}
	public void w(String str)
	{
		if(isDebug == true && tag!=null)
		Log.w(tag, str);		
	}
	public void d(String str)
	{
		if(isDebug == true && tag!=null)
		Log.d(tag, str);		
	}
	public void e(String str)
	{
		if(isDebug == true && tag!=null)
		Log.e(tag, str);		
	}

}
