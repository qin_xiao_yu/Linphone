package com.sxkeda.voip;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import org.linphone.ContactsManager;
import org.linphone.LinphoneActivity;
import org.linphone.LinphoneContact;
import org.linphone.LinphoneManager;
import org.linphone.LinphonePreferences;
import org.linphone.LinphoneService;
import org.linphone.LinphoneUtils;
import org.linphone.R;
import org.linphone.core.CallDirection;
import org.linphone.core.LinphoneAddress;
import org.linphone.core.LinphoneAuthInfo;
import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCallLog;
import org.linphone.core.LinphoneCallParams;
import org.linphone.core.LinphoneCallStats;
import org.linphone.core.LinphoneChatMessage;
import org.linphone.core.LinphoneChatRoom;
import org.linphone.core.LinphoneCore;
import org.linphone.core.LinphoneCoreException;
import org.linphone.core.LinphoneCoreListenerBase;
import org.linphone.core.LinphoneProxyConfig;
import org.linphone.core.PayloadType;
import org.linphone.core.Reason;
import org.linphone.core.LinphoneCall.State;
import org.linphone.core.LinphoneCallLog.CallStatus;
import org.linphone.core.LinphoneCore.RegistrationState;
import org.linphone.mediastream.Log;

import com.qinxiaoyu.lib.util.format.string.json.Json;
import com.sxkeda.voip.Business.CFG.KedaLinphoneParam;
import com.sxkeda.voip.event.Operater;

import android.R.bool;
import android.R.integer;
import android.content.Context;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;
import android.widget.TextView;

public class Business
{

	private static Business instance;
	private Context mContext;
	private LinphoneServer kedaLinphoneServer;

	private LinphoneCoreListenerBase mListener;
	private LinphoneCall.State callStatus = LinphoneCall.State.CallReleased;
	private LinphoneCore.RegistrationState registrationState = LinphoneCore.RegistrationState.RegistrationNone;
	private boolean newProxyConfig;
	private String callNumberTo = "1006";

	private AccountRegist kedaAccountRegist;

	private int callTime;
	private Handler handler;
	private Thread callTimeTimerThread;
	private boolean callTimeFlag = false;

	private AudioManager mAudioManager;

	private boolean isAllowVideoOrAudioCall = false;

	private Debug debug = new Debug(Debug.isKedaBusiness, "KedaBusiness");

	private static final int HANDLER_CALL_TIME = 1;
	private static final int HANDLER_REGATION_STATUS = 3;
	private static final int HANDLER_CALL_STATUS = 4;
	private static final int HANDLER_CALL_NUMBER = 5;
	private static final int HANDLER_CALL_INFORMATION = 6;

	private String basePath,kedaLinphoneCfgFile;
	private Resources mR;
	
	private KedaLinphoneParam param;
	private List<LinphoneCallLog> mLogs;
	
	public Business(Context context)
	{
		debug.i("KedaBusiness");
		this.mContext = context;
		// callTimeTimer = new Timer();
		instance = this;
		mAudioManager = ((AudioManager) this.mContext.getSystemService(Context.AUDIO_SERVICE));
		
		basePath = context.getFilesDir().getAbsolutePath();
		kedaLinphoneCfgFile = basePath + "/kedaCfg";
		CFG cfg = new CFG(kedaLinphoneCfgFile);
		param = cfg.getCfg();
		if(param==null) return;
			
		
		
		kedaLinphoneServer = new LinphoneServer(this.mContext);
		ServerReadyDetectThread serverReadyDetectThread = new ServerReadyDetectThread();
		serverReadyDetectThread.start();
		
		debug.i("handler = new Handler");
		handler = new Handler()
		{

			public void handleMessage(Message msg)
			{
				switch(msg.what)
				{
				case HANDLER_CALL_TIME:
				{
					Bundle bundle = new Bundle();
					bundle = msg.getData();
					Operater.getInstance().setCallTime(bundle.getString("KEY"));
				}
					break;
				case HANDLER_REGATION_STATUS:
				{
					Bundle bundle = new Bundle();
					bundle = msg.getData();
					Operater.getInstance().setRegationStatus(bundle.getString("KEY"));
				}
					break;
				case HANDLER_CALL_STATUS:
				{
					Bundle bundle = new Bundle();
					bundle = msg.getData();
					LinphoneCall.State state = (LinphoneCall.State) bundle.getSerializable("KEY");					
					Operater.getInstance().setCallStatus(state);
				}
					break;
				case HANDLER_CALL_NUMBER:
				{
					Bundle bundle = new Bundle();
					bundle = msg.getData();
					Operater.getInstance().setCallNumber(bundle.getString("KEY"));
				}
					break;
				case HANDLER_CALL_INFORMATION:
				{
					Bundle bundle = new Bundle();
					bundle = msg.getData();
					CallInformation callInformation = (CallInformation) bundle.getSerializable("KEY");
					Operater.getInstance().setCallInformation(callInformation);
				}
					break;
				}
			}
		};
		
	}

	public static synchronized final Business getInstance()
	{
		if (instance != null) return instance;
		throw new RuntimeException("KedaBusiness should be created before accessed");
	}

	/********************************** Linphone *************************************/
	private void initLinphoneCoreListener()
	{
		debug.i("initLinphoneCoreListener");
		mListener = new LinphoneCoreListenerBase()
		{
			@Override
			public void messageReceived(LinphoneCore lc, LinphoneChatRoom cr, LinphoneChatMessage message)
			{
				debug.i("messageReceived");
				// displayMissedChats(getUnreadMessageCount());
			}

			@Override
			public void authInfoRequested(LinphoneCore lc, String realm, String username, String domain)
			{
				debug.i("authInfoRequested");
				// authInfoPassword = displayWrongPasswordDialog(username,
				// realm, domain);
				// authInfoPassword.show();
			}

			@Override
			public void registrationState(LinphoneCore lc, LinphoneProxyConfig proxy, LinphoneCore.RegistrationState state, String smessage)
			{

				debug.i("registrationState smessage = " + smessage);
				debug.i("proxy.getIdentity() = " + proxy.getIdentity());
				debug.i("proxy.getRealm() = " + proxy.getRealm());
				debug.i("proxy.getDomain() = " + proxy.getDomain());
				sendMessage(HANDLER_REGATION_STATUS, state.toString());
				registrationState = state;
				debug.i("registrationState = "+registrationState.toString());
				
				// if (state.equals(RegistrationState.RegistrationOk))
				// Operater.getInstance().setLinphoneServiceStatus(true);
				// registrationStateTextView.setText(smessage);

				if (state.equals(RegistrationState.RegistrationCleared))
				{
					if (lc != null)
					{
						LinphoneAuthInfo authInfo = lc.findAuthInfo(proxy.getIdentity(), proxy.getRealm(), proxy.getDomain());
						if (authInfo != null) lc.removeAuthInfo(authInfo);
					}
				}

				// refreshAccounts();

				if (state.equals(RegistrationState.RegistrationFailed) && newProxyConfig)
				{
					newProxyConfig = false;
					if (proxy.getError() == Reason.BadCredentials)
					{
						// displayCustomToast(getString(R.string.error_bad_credentials),
						// Toast.LENGTH_LONG);
					}
					if (proxy.getError() == Reason.Unauthorized)
					{
						// displayCustomToast(mContext.getString(R.string.error_unauthorized),
						// Toast.LENGTH_LONG);
					}
					if (proxy.getError() == Reason.IOError)
					{
						// displayCustomToast(mContext.getString(R.string.error_io_error),
						// Toast.LENGTH_LONG);
					}
				}
			}

			@Override
			public void callState(LinphoneCore lc, LinphoneCall call, LinphoneCall.State state, String message)
			{
				debug.i("callStatus(" + state.toString() + ")");
				callStatus = call.getState();
				// Operater.getInstance().setCallStatus(message);
				sendMessage(HANDLER_CALL_STATUS, state);

				if (state == State.IncomingReceived)
				{
					debug.i("State.IncomingReceived");
					debug.i("isVideoEnabled(" + isVideoEnabled(call) + ");");
					sendMessage(HANDLER_CALL_NUMBER, call.getRemoteAddress().asStringUriOnly());
					// autoAnswer(call);
					// answer();
					CallApi.answer(mContext);
				}
				else if (state == State.StreamsRunning)
				{
					LinphoneManager.getLc().enableSpeaker(LinphoneManager.getLc().isSpeakerEnabled());
					registerCallDurationTimer(call);
				}
				else if (state == State.OutgoingInit || state == State.OutgoingProgress)
				{
					
				}
				else if (state == State.CallEnd || state == State.Error || state == State.CallReleased)
				{
					debug.i("(state == State.CallEnd || state == State.Error || state == State.CallReleased)");
					sendMessage(HANDLER_CALL_TIME, "");
					sendMessage(HANDLER_CALL_NUMBER, "");

					callTimeFlag = false;
					callTimeTimerThread = null;

					if (message != null && call.getErrorInfo().getReason() == Reason.Declined)
					{

					}
					else if (message != null && call.getErrorInfo().getReason() == Reason.NotFound)
					{

					}
					else if (message != null && call.getErrorInfo().getReason() == Reason.Media)
					{

					}
					else if (message != null && state == State.Error)
					{

					}
				}

			}
		};
		LinphoneCore lc = LinphoneManager.getLcIfManagerNotDestroyedOrNull();
		if (lc != null)
		{
			debug.i("LinphoneCore lc is not null and addListener");
			lc.addListener(mListener);
		}
		else
		{
			debug.w("LinphoneCore lc is null");
		}

	}
	public LinphoneCore.RegistrationState getRegistrationState()
	{
		return registrationState;
	}
	
	public LinphoneCall.State getCallStatus()
	{
		if (LinphoneManager.getLc().getCalls().length > 0)
		{
			LinphoneCall call = LinphoneManager.getLc().getCalls()[0];
			callStatus = call.getState();
		}
		return callStatus;
	}

	private boolean isVideoEnabled(LinphoneCall call)
	{
		if (call != null)
		{
			return call.getCurrentParamsCopy().getVideoEnabled();
		}
		return false;
	}

	public void setPreference()
	{
		LinphonePreferences mPrefs = LinphonePreferences.instance();
		mPrefs.setAutomaticallyAcceptVideoRequests(true);
		// mPrefs.setBandwidthLimit(380);
		mPrefs.setInitiateVideoCall(true);
		mPrefs.setDebugEnabled(true);
		LinphoneManager.getLc().setMaxCalls(0);
		mPrefs.enableVideo(true);
		// mPrefs.setFrontCamAsDefault(false);
	}
	public void setMaxCalls(int count)
	{
		LinphoneManager.getLc().setMaxCalls(count);
	}
 
	public void getPreference()
	{
		LinphonePreferences mPrefs = LinphonePreferences.instance();

		debug.i("getVideoPreset = " + mPrefs.getVideoPreset());
		debug.i("isVideoEnabled = " + mPrefs.isVideoEnabled());
		debug.i("shouldInitiateVideoCall = " + mPrefs.shouldInitiateVideoCall());
		debug.i("shouldAutomaticallyAcceptVideoRequests = " + mPrefs.shouldAutomaticallyAcceptVideoRequests());
		debug.i("getVideoPreset = " + mPrefs.getVideoPreset());
		debug.i("getPreferredVideoSize = " + mPrefs.getPreferredVideoSize());
		debug.i("getPreferredVideoFps = " + mPrefs.getPreferredVideoFps());
		debug.i("getBandwidthLimit = " + mPrefs.getBandwidthLimit());

		debug.i("getMaxCalls = " + LinphoneManager.getLc().getMaxCalls());
   
	}
 
	/********************************** LinphoneServer *************************************/
	private void kedaLinphoneServerReady()
	{
		debug.i("kedaLinphoneServerReady");
		initLinphoneCoreListener();
		kedaAccountRegist = new AccountRegist(mContext,param.id, param.passwd, param.service, param.id);
		// operater.setLinphoneServiceStatus(true);
		setPreference();
		getPreference();
		setCodecMime();
		getCodecMime();
		Operater.getInstance().setLinphoneServiceStatus(true);
		refreshHistoryCalls();
		// handler.sendEmptyMessage(HANDLER_LINPHONSERVICE_OK);
	}

	private class ServerReadyDetectThread extends Thread
	{

		@Override
		public void run()
		{
			// TODO Auto-generated method stub
			// super.run();
			while (!LinphoneService.isReady())
			{
				// debug.i("LinphoneService is not ready");
				try
				{
					sleep(30);
				}
				catch (InterruptedException e)
				{
					throw new RuntimeException("waiting thread sleep() has been interrupted");
				}
			}
			debug.i("LinphoneService ready");

			kedaLinphoneServerReady();
		}
	}

	public void callControlCenter()
	{
		CallApi.dialNumber(callNumberTo);
	}

	public void newVideoCall(String number)
	{
		debug.i("newVideoCall");
		LinphonePreferences mPrefs = LinphonePreferences.instance();
		// mPrefs.enableVideo(true);
		mPrefs.setInitiateVideoCall(true);
		CallApi.dialNumber(callNumberTo);
	}

	public void newAudioCall(String number)
	{
		debug.i("newAudioCall");
		LinphonePreferences mPrefs = LinphonePreferences.instance();
		// mPrefs.enableVideo(false);
		mPrefs.setInitiateVideoCall(false);
		CallApi.dialNumber(callNumberTo);
	}
	
	
	
	public void registerCallDurationTimer(LinphoneCall call)
	{

		callTime = 0;
		callTimeFlag = true;
		if (callTimeTimerThread == null)
		{
			callTimeTimerThread = new Thread(new Runnable()
			{
				@Override
				public void run()
				{
					// TODO Auto-generated method stub
					while (callTimeFlag)
					{
						callTime++;
						sendCallTimeMessage(callTime);
						try
						{
							Thread.sleep(1000);
						}
						catch (InterruptedException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						getCallParam();
					}
				}
			});
		}
		if (callTimeTimerThread != null && !callTimeTimerThread.isAlive()) callTimeTimerThread.start();

	}

	private void sendCallTimeMessage(int callTime)
	{
		Message msg = new Message();
		msg.what = HANDLER_CALL_TIME;
		Bundle bundle = new Bundle();
		if (callTime < 0)
		{
			bundle.putString("KEY", "");
		}
		else
		{
			bundle.putString("KEY", convert(callTime));
		}
		msg.setData(bundle);
		handler.sendMessage(msg);
	}

	private void sendMessage(int what, String message)
	{
		if (message == null) return;
		Message msg = new Message();
		msg.what = what;
		Bundle bundle = new Bundle();
		bundle.putString("KEY", message);
		msg.setData(bundle);
		handler.sendMessage(msg);
	}

	private void sendMessage(int what, CallInformation callInformation)
	{
		if (callInformation == null) return;
		Message msg = new Message();
		msg.what = what;
		Bundle bundle = new Bundle();
		bundle.putSerializable("KEY", callInformation);
		msg.setData(bundle);
		handler.sendMessage(msg);
	}
	private void sendMessage(int what, LinphoneCall.State state)
	{
		if (state == null) return;
		Message msg = new Message();
		msg.what = what;
		Bundle bundle = new Bundle();
		bundle.putSerializable("KEY", state);
		msg.setData(bundle);
		handler.sendMessage(msg);
	}

	public static String convert(int second)
	{
		int h = 0, d = 0, s = 0;
		s = second % 60;
		second = second / 60;
		d = second % 60;
		h = second / 60;
		return h + "时" + d + "分" + s + "秒";
	}

	private void getCallParam()
	{

		LinphoneCall call = LinphoneManager.getLc().getCurrentCall();

		if (call == null)
		{
			debug.i("current call is null");
			return;
		}

		CallInformation callInformation = new CallInformation();
		float signalQuality = call.getCurrentQuality();
		callInformation.setSignalQuality(signalQuality);

		final LinphoneCallParams params = call.getCurrentParamsCopy();
		if (params.getVideoEnabled())
		{
			final LinphoneCallStats videoStats = call.getVideoStats();
			final LinphoneCallStats audioStats = call.getAudioStats();
			if (videoStats != null && audioStats != null)
			{
				PayloadType payloadAudio = params.getUsedAudioCodec();
				PayloadType payloadVideo = params.getUsedVideoCodec();
				if (payloadVideo != null && payloadAudio != null)
				{
					callInformation.setVideoMine(payloadVideo.getMime());
					callInformation.setAudioMine(payloadAudio.getMime());
				}

				callInformation.setvideo(
						String.valueOf((int) audioStats.getDownloadBandwidth()), 
						String.valueOf((int) audioStats.getUploadBandwidth()),
				        String.valueOf((int) videoStats.getDownloadBandwidth()), 
				        String.valueOf((int) videoStats.getUploadBandwidth()));
				sendMessage(HANDLER_CALL_INFORMATION, callInformation);
			}
		}
		else
		{
			final LinphoneCallStats audioStats = call.getAudioStats();
			if (audioStats != null)
			{
				PayloadType payload = params.getUsedAudioCodec();
				if (payload != null)
				{
					callInformation.setAudioMine(payload.getMime());
				}
				callInformation.setAudio(
						String.valueOf((int) audioStats.getDownloadBandwidth()), 
						String.valueOf((int) audioStats.getUploadBandwidth()));
				sendMessage(HANDLER_CALL_INFORMATION, callInformation);
			}
		}

	}

	public class CallInformation implements Serializable
	{
		/***/
		private static final long serialVersionUID = 1L;
		private String type;
		private String audioDownloadBandwidth = "";
		private String audioUploadBandwidth = "";
		private String videoDownloadBandwidth = "";
		private String videoUploadBandwidth = "";
		private String audioMine = "";
		private String videoMine = "";
		private float signalQuality;

		public CallInformation()
		{
		}

		public void setAudio(String audioDownloadBandwidth, String audioUploadBandwidth)
		{
			type = "audio";
			this.audioDownloadBandwidth = audioDownloadBandwidth;
			this.audioUploadBandwidth = audioUploadBandwidth;
		}

		public void setvideo(String audioDownloadBandwidth, String audioUploadBandwidth, String videoDownloadBandwidth, String videoUploadBandwidth)
		{
			type = "video";
			this.audioDownloadBandwidth = audioDownloadBandwidth;
			this.audioUploadBandwidth = audioUploadBandwidth;
			this.videoDownloadBandwidth = videoDownloadBandwidth;
			this.videoUploadBandwidth = videoUploadBandwidth;
		}

		public float getSignalQuality()
		{
			return signalQuality;
		}

		public void setSignalQuality(float signalQuality)
		{
			this.signalQuality = signalQuality;
		}

		public void setAudioMine(String audioMine)
		{
			this.audioMine = audioMine;
		}

		public void setVideoMine(String videoMine)
		{
			this.videoMine = videoMine;
		}

		public String getCallType()
		{
			return type;
		}

		public String getAudioDownloadBandwidth()
		{
			return audioDownloadBandwidth;
		}

		public String getAudioUploadBandwidth()
		{
			return audioUploadBandwidth;
		}

		public String getVideoDownloadBandwidth()
		{
			return videoDownloadBandwidth;
		}

		public String getVideoUploadBandwidth()
		{
			return videoUploadBandwidth;
		}

		public String getAudioMine()
		{
			return audioMine;
		}

		public String getVideoMine()
		{
			return videoMine;
		}

	}

	private void setCodecMime()
	{
		LinphoneCore lc = LinphoneManager.getLcIfManagerNotDestroyedOrNull();
		for (final PayloadType pt : lc.getVideoCodecs())
		{
			debug.i("setCodecMime = " + pt.getMime());
			if (!pt.getMime().equals("VP8"))
			{
				try
				{
					debug.i("disable codec " + pt.getMime());
					LinphoneManager.getLcIfManagerNotDestroyedOrNull().enablePayloadType(pt, false);
				}
				catch (LinphoneCoreException e)
				{
					Log.e(e);
				}
			}
		}
	}

	private void getCodecMime()
	{
		LinphoneCore lc = LinphoneManager.getLcIfManagerNotDestroyedOrNull();
		for (final PayloadType pt : lc.getVideoCodecs())
		{
			debug.i("getCodecMime = " + pt.getMime());
		}
	}

	public void setIsAllowVideoOrAudioCall(boolean enable)
	{
		debug.i("setIsAllowVideoOrAudioCall(" + enable + ")");
		this.isAllowVideoOrAudioCall = enable;
		debug.i("isAllowVideoOrAudioCall = " + isAllowVideoOrAudioCall);
		LinphonePreferences mPrefs = LinphonePreferences.instance();
		mPrefs.setAutomaticallyAcceptVideoRequests(isAllowVideoOrAudioCall);
	}

	public boolean isAllowVideoOrAudioCall()
	{
		return isAllowVideoOrAudioCall;
	}

	public boolean isCurrentCallVideo()
	{
		if (isVideoEnabled(LinphoneManager.getLc().getCurrentCall()))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void copyIfNotExist(int ressourceId, String target) throws IOException {
		debug.i("copyIfNotExist");
		File lFileToCopy = new File(target);
		if (!lFileToCopy.exists()) {
			copyFromPackage(ressourceId,lFileToCopy.getName());
		}
	}

	public void copyFromPackage(int ressourceId, String target) throws IOException{
		debug.i("copyFromPackage");
		FileOutputStream lOutputStream = mContext.openFileOutput (target, 0);
		InputStream lInputStream = mR.openRawResource(ressourceId);
		int readByte;
		byte[] buff = new byte[8048];
		while (( readByte = lInputStream.read(buff)) != -1) {
			lOutputStream.write(buff,0, readByte);
		}
		lOutputStream.flush();
		lOutputStream.close();
		lInputStream.close();
	}
//	private void copyAssetsFromPackage() throws IOException {
//		debug.i("copyAssetsFromPackage");
//		copyIfNotExist(R.raw.oldphone_mono, mRingSoundFile);
//		copyIfNotExist(R.raw.ringback, mRingbackSoundFile);
//		copyIfNotExist(R.raw.hold, mPauseSoundFile);
//		copyIfNotExist(R.raw.incoming_chat, mErrorToneFile);
//		copyIfNotExist(R.raw.linphonerc_default, mLinphoneConfigFile);
//		copyFromPackage(R.raw.linphonerc_factory, new File(mLinphoneFactoryConfigFile).getName());
//		copyIfNotExist(R.raw.lpconfig, mLPConfigXsd);
//		copyIfNotExist(R.raw.rootca, mLinphoneRootCaFile);
//	}
	/**
	 * 获取当前通话的姓名
	 * @author    秦晓宇
	 * @date      2016年7月18日 上午11:42:42 
	 * @return
	 */
	public String getCallName() throws NullPointerException
	{
		String name = null;
		
		if(LinphoneManager.getLc().getCallsNb() <= 0)
		{
			return name;
		}
		else
		{
			LinphoneCall call = LinphoneManager.getLc().getCurrentCall();
			LinphoneAddress lAddress = call.getRemoteAddress();
			LinphoneContact lContact = ContactsManager.getInstance().findContactFromAddress(lAddress);
			if (lContact == null)
			{
				name = LinphoneUtils.getAddressDisplayName(lAddress);
			}
			else
			{
				name = lContact.getFullName();
			}
		}
		return name;
	}
	
	
	
	class CFG
	{
		class KedaLinphoneParam{
			public String id;
			public String passwd;
			public String service;
			public String centerNumber;
		}
		public String filePath;
		public CFG(String path)
		{
			this.filePath = path;		
		} 
		public KedaLinphoneParam getCfg()
		{ 
			debug.i("filePath = "+filePath);
			File lFileToCopy = new File(filePath);
			boolean isexit = lFileToCopy.exists();
			debug.i("exists = "+isexit); 
			if (isexit == false) 
			{
				debug.i("cfg file exit");
				KedaLinphoneParam param = new KedaLinphoneParam();
				param.id = "1200";
				param.passwd = "1234";
				param.service = "192.168.1.33";
				param.centerNumber = "1006";
				String cfg = Json.toJsonByPretty(param);
				if(com.qinxiaoyu.lib.util.file.File.write(this.filePath, cfg, false) == false)
				{
					debug.i("write cfg files false");
				}  
				return param;
			}
			else
			{
				debug.i("cfg file not exit");
				String cfgString = com.qinxiaoyu.lib.util.file.File.read(this.filePath);
				if(cfgString == null) return null;
				KedaLinphoneParam param = new KedaLinphoneParam();
				param = (KedaLinphoneParam) Json.toObject(param, cfgString);
				return param;
			}
		}

	}
	/**
	 * 获取历史电话的数量
	 * @author    秦晓宇
	 * @date      2016年7月19日 上午11:39:56 
	 * @return
	 */
	public int getHistoryCallsSize()
	{
		refreshHistoryCalls();
		if(mLogs!=null) return mLogs.size();
		return 0;		
	}
	/**
	 * 获得未接来电的数量
	 * @author    秦晓宇
	 * @date      2016年7月19日 上午11:42:35 
	 * @return
	 */
	public int getMissedCallsCount()
	{
//		int missCallCount = 0;
//		refreshHistoryCalls();
		
		return LinphoneManager.getLc().getMissedCallsCount();
//		if (mLogs != null && mLogs.size() > 0)
//		{
//			for (LinphoneCallLog log : mLogs)
//			{
//				if (log.getStatus() == CallStatus.Missed)
//				{
//					missCallCount++;
//				}
//			}
//			return missCallCount;
//		}
//		else
//		{
//			return 0;
//		}
	}
	
	/**
	 * 获取第一个未接来电
	 * @author    秦晓宇
	 * @date      2016年7月19日 上午11:39:36 
	 * @return
	 * @throws NullPointerException
	 */
	public LinphoneCallLog getFirstMissCall() throws NullPointerException
	{
		refreshHistoryCalls();
		if (mLogs != null && mLogs.size() > 0)
		{
			LinphoneCallLog log = mLogs.get(0);
			if (log.getDirection() == CallDirection.Incoming)
			{
				return mLogs.get(0);
			}
			else
				return null;
		}
		else
		{
			return null;
		}
	}

	/**
	 * 刷新所有历史电话
	 * @author    秦晓宇
	 * @date      2016年7月19日 上午11:39:04 
	 */
	public void refreshHistoryCalls()
	{
		mLogs = Arrays.asList(LinphoneManager.getLc().getCallLogs());
	}
	
	/**
	 * 获得所有的历史电话
	 * @author    秦晓宇
	 * @date      2016年7月19日 上午11:51:09 
	 * @return
	 * @throws NullPointerException
	 */
	public List<LinphoneCallLog> getHistoryCalls() throws NullPointerException
	{
		refreshHistoryCalls();
		return mLogs;
	}
	
	/**
	 * 显示所有历史电话
	 * @author    秦晓宇
	 * @date      2016年7月19日 下午2:44:13 
	 */
	public void displayHistotyCalls()
	{		
		try
        {
	        List<LinphoneCallLog> calls = Arrays.asList(LinphoneManager.getLc().getCallLogs());
	        if (calls != null && calls.size() > 0)
			{
				for (LinphoneCallLog log : calls)
				{
					debug.i(/*"id = "+log.getCallId()
							+*/" state = "+log.getDirection().toString()
							+" from = "+log.getFrom()
							/*+" getTimestamp = "+log.getTimestamp()
							+" getStartDate = "+log.getStartDate()*/);
				}
			}      
        }
        catch (Exception e)
        {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }

		
	}
	
//	public boolean clearHistoryCalls()
//	{
//		boolean ret = false;
//		refreshHistoryCalls();
//	}
	
	
}
