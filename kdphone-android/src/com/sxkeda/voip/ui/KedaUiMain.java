package com.sxkeda.voip.ui;

import org.linphone.LinphoneManager;
import org.linphone.R;
import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCore;
import org.linphone.mediastream.video.AndroidVideoWindowImpl;

import com.sxkeda.voip.Debug;
import com.sxkeda.voip.Business;
import com.sxkeda.voip.Business.CallInformation;
import com.sxkeda.voip.CallApi;
import com.sxkeda.voip.event.Listener;
import com.sxkeda.voip.event.Operater;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

public class KedaUiMain extends Activity
{

	Business kedaBusiness;
	private TextView textView1, textView2, textView3, textView5, textView6, textView7, textView8, textView9;
	private Button button1, button2, button3, button4, button5;
	private SurfaceView captureSurfaceView, surfaceView1;
	private ToggleButton toggleBtn1;
	private AndroidVideoWindowImpl androidVideoWindowImpl;
	private Debug debug = new Debug(Debug.isKedaUiMain, "KedaUiMain");
	private Operater operater;
	private Listener mListener;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.keda_ui_main);
		operater = new Operater();
		initViews();

		kedaBusiness = new Business(getApplicationContext());

		if (!LinphoneManager.isInstanciated())
		{
			debug.i("!LinphoneManager.isInstanciated()");
		}

		mListener = new Listener()
		{

			@Override
			public void updateRegationStatus(String string)
			{
				// TODO Auto-generated method stub
				textView1.setText(string);
			}

			@Override
			public void updataLinphoneServiceStatus(boolean status)
			{
				// TODO Auto-generated method stub
				setVideoSurfaceView();
			}

			@Override
			public void updataCallTime(String time)
			{
				// TODO Auto-generated method stub
				textView5.setText(time);
			}

			@Override
			public void updataCallStatus(LinphoneCall.State state)
			{
				// TODO Auto-generated method stub
				textView2.setText(state.toString());
				// state == State.CallEnd || state == State.Error || state ==
				// State.CallReleased
				if(state == LinphoneCall.State.CallReleased)
				{
					if (captureSurfaceView != null) captureSurfaceView = null;
					if (surfaceView1 != null) surfaceView1 = null;
					if (androidVideoWindowImpl != null)
					{
						// Prevent linphone from crashing if correspondent hang
						// up while you are rotating
						androidVideoWindowImpl.release();
						androidVideoWindowImpl = null;
					}
				}
				else if(state == LinphoneCall.State.StreamsRunning)
				{
					setVideoSurfaceView();
				}
				
				// if (string.equals("CallReleased"))
				// {
				// if (captureSurfaceView != null) captureSurfaceView = null;
				// if (surfaceView1 != null) surfaceView1 = null;
				// if (androidVideoWindowImpl != null)
				// {
				// // Prevent linphone from crashing if correspondent hang
				// // up while you are rotating
				// androidVideoWindowImpl.release();
				// androidVideoWindowImpl = null;
				// }
				// }
				// else if (string.equals("StreamsRunning"))
				// {
				// setVideoSurfaceView();
				// }
			}

			@Override
			public void updataCallNumber(String number)
			{
				// TODO Auto-generated method stub
				textView3.setText(number);
			}

			@Override
			public void updataCallInformation(CallInformation callInformation)
			{
				// TODO Auto-generated method stub
				// debug.i("call type = "+callInformation.getCallType());
				// debug.i("getAudioDownloadBandwidth = "+callInformation.getAudioDownloadBandwidth());
				// debug.i("getAudioUploadBandwidth = "+callInformation.getAudioUploadBandwidth());
				// debug.i("getVideoDownloadBandwidth = "+callInformation.getVideoDownloadBandwidth());
				// debug.i("getVideoUploadBandwidth = "+callInformation.getVideoUploadBandwidth());

				textView9.setText("" + callInformation.getSignalQuality());
				if (callInformation.getCallType().equals("video"))
				{
					textView8.setText(callInformation.getAudioMine() + "/" + callInformation.getVideoMine());
					textView6.setText("��Ƶ" + callInformation.getAudioDownloadBandwidth() + "/" + callInformation.getAudioDownloadBandwidth() + " kbits/s");
					textView7.setText("��Ƶ" + callInformation.getVideoDownloadBandwidth() + "/" + callInformation.getVideoUploadBandwidth() + " kbits/s");

				}
				else if (callInformation.getCallType().equals("audio"))
				{
					textView6.setText("��Ƶ" + callInformation.getAudioDownloadBandwidth() + "/" + callInformation.getAudioDownloadBandwidth() + " kbits/s");
					textView7.setText("");
					textView8.setText(callInformation.getAudioMine());
				}

			}
		};
		Operater.getInstance().addListener(mListener);
	}

	private void setVideoSurfaceView()
	{
		debug.i("updataLinphoneServiceStatus");
		captureSurfaceView.getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		// Warning useless because value is ignored and automatically set by new
		// APIs.
		fixZOrder(surfaceView1, captureSurfaceView);
		androidVideoWindowImpl = new AndroidVideoWindowImpl(captureSurfaceView, surfaceView1, new AndroidVideoWindowImpl.VideoWindowListener()
		{
			public void onVideoRenderingSurfaceReady(AndroidVideoWindowImpl vw, SurfaceView surface)
			{
				debug.i("onVideoRenderingSurfaceReady");
				surfaceView1 = surface;
				LinphoneManager.getLc().setVideoWindow(vw);
			}

			public void onVideoRenderingSurfaceDestroyed(AndroidVideoWindowImpl vw)
			{
				debug.i("onVideoRenderingSurfaceDestroyed");
				LinphoneCore lc = LinphoneManager.getLc();
				if (lc != null)
				{
					lc.setVideoWindow(null);
				}
			}

			public void onVideoPreviewSurfaceReady(AndroidVideoWindowImpl vw, SurfaceView surface)
			{
				debug.i("onVideoPreviewSurfaceReady");
				debug.i("surface = " + surface.toString());
				captureSurfaceView = surface;
				LinphoneManager.getLc().setPreviewWindow(captureSurfaceView);
			}

			public void onVideoPreviewSurfaceDestroyed(AndroidVideoWindowImpl vw)
			{
				debug.i("onVideoPreviewSurfaceDestroyed");
				// Remove references kept in jni code and restart camera
				LinphoneManager.getLc().setPreviewWindow(null);
			}
		});
		if (androidVideoWindowImpl != null)
		{
			debug.i("androidVideoWindowImpl != null");
			synchronized (androidVideoWindowImpl)
			{
				LinphoneManager.getLc().setVideoWindow(androidVideoWindowImpl);
			}
		}
		debug.i("updataLinphoneServiceStatus");
	}

	private void initViews()
	{
		debug.i("initViews");
		textView1 = (TextView) findViewById(R.id.textView1);
		textView2 = (TextView) findViewById(R.id.textView2);
		textView3 = (TextView) findViewById(R.id.textView3);
		textView5 = (TextView) findViewById(R.id.textView5);
		textView6 = (TextView) findViewById(R.id.textView6);
		textView7 = (TextView) findViewById(R.id.textView7);
		textView8 = (TextView) findViewById(R.id.textView8);
		textView9 = (TextView) findViewById(R.id.textView9);

		button1 = (Button) findViewById(R.id.button1);
		button2 = (Button) findViewById(R.id.button2);
		button3 = (Button) findViewById(R.id.button3);
		button4 = (Button) findViewById(R.id.button4);
		button5 = (Button) findViewById(R.id.button5);

		toggleBtn1 = (ToggleButton) findViewById(R.id.toggleButton1);

		captureSurfaceView = (SurfaceView) findViewById(R.id.captureSurfaceView2);
		surfaceView1 = (SurfaceView) findViewById(R.id.surfaceView2);

		button1.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				debug.i("callControlCenter");
				Business.getInstance().callControlCenter();
			}
		});
		button2.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				debug.i("answer");
				// KedaBusiness.getInstance().answer();
				CallApi.answer(getApplicationContext());
			}
		});
		button3.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				debug.i("hangUp");
				CallApi.hangUp();
			}
		});
		button4.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				debug.i("call audio");
				Business.getInstance().newAudioCall("1234");
			}
		});
		button5.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				debug.i("call video");
				Business.getInstance().newVideoCall("1234");
			}
		});

		toggleBtn1.setOnCheckedChangeListener(new ToggleButton.OnCheckedChangeListener()
		{

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
			{
				// TODO Auto-generated method stub
				if (isChecked)
				{
					Business.getInstance().setIsAllowVideoOrAudioCall(true);
				}
				else
				{
					Business.getInstance().setIsAllowVideoOrAudioCall(false);
				}
			}

		});
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		// TODO Auto-generated method stub
		debug.i("onKeyDown = " + keyCode);
		// debug.i("callState = "+callState.toString());

		if (keyCode == 82)
		{
			Business.getInstance().callControlCenter();
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event)
	{
		// TODO Auto-generated method stub
		return super.onKeyUp(keyCode, event);
	}

	private void fixZOrder(SurfaceView video, SurfaceView preview)
	{
		video.setZOrderOnTop(false);
		preview.setZOrderOnTop(true);
		preview.setZOrderMediaOverlay(true); // Needed to be able to display
		                                     // control layout over
	}

	@Override
	protected void onDestroy()
	{
		debug.i("onDestroy");
		// TODO Auto-generated method stub
		if (captureSurfaceView != null) captureSurfaceView = null;
		if (surfaceView1 != null) surfaceView1 = null;
		if (androidVideoWindowImpl != null)
		{
			// Prevent linphone from crashing if correspondent hang up while you
			// are rotating
			androidVideoWindowImpl.release();
			androidVideoWindowImpl = null;
		}

		super.onDestroy();
		// System.gc();
	}

	@Override
	public void onResume()
	{
		super.onResume();
		debug.i("onResume");
		if (androidVideoWindowImpl != null)
		{
			synchronized (androidVideoWindowImpl)
			{
				LinphoneManager.getLc().setVideoWindow(androidVideoWindowImpl);
			}
		}
	}

	@Override
	public void onPause()
	{
		debug.i("onPause");
		if (androidVideoWindowImpl != null)
		{
			synchronized (androidVideoWindowImpl)
			{
				/*
				 * this call will destroy native opengl renderer which is used
				 * by
				 * androidVideoWindowImpl
				 */
				LinphoneManager.getLc().setVideoWindow(null);
			}
		}

		super.onPause();
	}

}
