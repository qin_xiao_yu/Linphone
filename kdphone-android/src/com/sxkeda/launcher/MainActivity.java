package com.sxkeda.launcher;

import java.util.Arrays;
import java.util.List;

import org.linphone.CallVideoFragment;
import org.linphone.LinphoneManager;
import org.linphone.R;
import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCallLog;
import org.linphone.core.LinphoneCallParams;

import com.sxkeda.launcher.component.LeftIconPanel;
import com.sxkeda.launcher.component.LeftIconPanel.ICON_INDEX;
import com.sxkeda.launcher.component.TitleBar;
import com.sxkeda.launcher.component.LeftIconPanel.ICoallBack;
import com.sxkeda.launcher.fragment.AudioCallFragment;
import com.sxkeda.launcher.fragment.CallFragment;
import com.sxkeda.launcher.fragment.IrisRecognitionFragment;
import com.sxkeda.launcher.fragment.MapFragment;
import com.sxkeda.launcher.fragment.WebViewFragment;
import com.sxkeda.voip.Business;
import com.sxkeda.voip.CallApi;
import com.sxkeda.voip.Business.CallInformation;
import com.sxkeda.voip.event.Listener;
import com.sxkeda.voip.event.Operater;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends Activity implements OnClickListener
{

	private LeftIconPanel left_icon_panel;
	private TitleBar titleBar;


	private Listener mListener;

	private LinphoneCall.State callState = LinphoneCall.State.CallReleased;
	
	private Debug debug = new Debug(Debug.isMainActivity, "MainActivity");
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		debug.i("onCreate");
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);//去掉信息栏
		setContentView(R.layout.activity_main);
		
		titleBar = (TitleBar)findViewById(R.id.titleBar);
		left_icon_panel = (LeftIconPanel)findViewById(R.id.left_icon_panel);
		left_icon_panel.setonClick(new ICoallBack()
		{
			@Override
			public void onClickButton(ICON_INDEX index)
			{
				// TODO Auto-generated method stub
				switch(index)
				{
					case CALL:
					{
						titleBar.setTitle("语音通话");
						changeFragmentToCall();
					}
					break;
					case MAP:
					{
						titleBar.setTitle("定位地图");
						changeFragmentToMap();
					}
					break;
					case PERSION:
						titleBar.setTitle("人员识别");
						changeFragmentToIris();
					break;
					case SETTING:
						titleBar.setTitle("参数配置");
					break;
					default:
					break;
				}
			}
		});
		
		
		titleBar.setTitle("语音通话");
		
		changeFragmentToCall();
		initListener();
		
		Business.getInstance().setMaxCalls(1);
		debug.i("HistoryCallsCount = "+Business.getInstance().getHistoryCallsSize());
		debug.i("getMissedCallsCount = "+Business.getInstance().getMissedCallsCount());
		Business.getInstance().displayHistotyCalls();
		
	}
	private void initListener()
	{
		mListener = new Listener()
		{

			@Override
			public void updateRegationStatus(String string)
			{
				debug.i("updateRegationStatus("+string+")");
			}

			@Override
			public void updataLinphoneServiceStatus(boolean status)
			{
				debug.i("updataLinphoneServiceStatus("+status+")");
			}

			@Override
			public void updataCallTime(String time)
			{
				debug.i("updataCallTime("+time+")");
			}

			@Override
			public void updataCallStatus(LinphoneCall.State state)
			{
				callState = state;
				if (state == LinphoneCall.State.Connected)
				{
					LinphoneCall mCall = LinphoneManager.getLc().getCurrentCall();
					final LinphoneCallParams remoteParams = mCall.getRemoteParams();
					debug.i("remoteParams.getVideoEnabled() = "+remoteParams.getVideoEnabled());
					if (remoteParams != null && remoteParams.getVideoEnabled())
					{
						changeFragmentToVideoCall();
					} 
					else
					{
						changeFragmentToAudioCall();
					}
				}

				debug.i("updataCallStatus(" + state.toString() + ")");
				if (state == LinphoneCall.State.OutgoingProgress)
				{
					debug.i("to CallVideoFragment");
					if (Business.getInstance().isCurrentCallVideo())
					{
						changeFragmentToVideoCall();
					}
					else
					{
						changeFragmentToAudioCall();
					}
				}				
				else if(state == LinphoneCall.State.CallEnd || state == LinphoneCall.State.Error || state == LinphoneCall.State.CallReleased){
					changeFragmentToCall();					
				}		
			}

			@Override
			public void updataCallNumber(String number)
			{
				debug.i("updataCallNumber("+number+")");
			}

			@Override
			public void updataCallInformation(CallInformation callInformation)
			{
				debug.i("updataCallInformation()");
			}
		};
		Operater.getInstance().addListener(mListener);
	}
	
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
	    // TODO Auto-generated method stub
		debug.i("keyCode = "+keyCode);
		switch(keyCode)  
	    {  
			case KeyEvent.KEYCODE_HOME:
			return true;
			case KeyEvent.KEYCODE_BACK:
//				debug.i("HistoryCallsCount = "+Business.getInstance().getHistoryCallsSize());
//				debug.i("getMissedCallsCount = "+Business.getInstance().getMissedCallsCount());
//				Business.getInstance().displayHistotyCalls();
//			return true;
				this.finish();
				break;
			case KeyEvent.KEYCODE_CALL:
			return true;
			case KeyEvent.KEYCODE_SYM:
			return true;
			case 82:
			{
				left_icon_panel.select();
			}
			return true;			
			case KeyEvent.KEYCODE_VOLUME_UP:
			{
				try
                {
					if(CallFragment.instance()!=null)
					{
						if(CallFragment.instance().isVisible())
						{
							debug.i("callFragment.setFocusNext()");
							CallFragment.instance().setFocusNext();
						}
					}
                }
                catch (RuntimeException e)
                {
	                // TODO: handle exception
                	e.printStackTrace();
                }
			}
			return true;
			case KeyEvent.KEYCODE_VOLUME_DOWN:
			{
				try
				{
					if (CallFragment.instance() != null)
					{
						if (CallFragment.instance().isVisible())
						{
							debug.i("callFragment.click()");
							CallFragment.instance().click();
						}
						else
						{
							if (callState != LinphoneCall.State.Idle || callState != LinphoneCall.State.CallEnd || callState != LinphoneCall.State.Error || callState != LinphoneCall.State.CallReleased)
							{
								debug.i("hangUp");
								CallApi.hangUp();
							}
						}

					}
					else
					{
						if (callState != LinphoneCall.State.Idle || callState != LinphoneCall.State.CallEnd || callState != LinphoneCall.State.Error || callState != LinphoneCall.State.CallReleased)
						{
							debug.i("hangUp");
							CallApi.hangUp();
						}
					}
				}
				catch (RuntimeException e)
				{
					e.printStackTrace();
				}
				
//				if (callFragment.isVisible())
//				{
//					debug.i("callFragment.click()");
//					callFragment.click();
//				}
//				else if (audioCallFragment.isVisible()) 
//					audioCallFragment.click();
//				else
//				{
//					if (callState != LinphoneCall.State.Idle || callState != LinphoneCall.State.CallEnd || callState != LinphoneCall.State.Error || callState != LinphoneCall.State.CallReleased)
//					{
//						debug.i("hangUp");
//						CallApi.hangUp();
//					}
//				}
			}
			return true;
			case KeyEvent.KEYCODE_STAR:
			return true;
		}
	    return super.onKeyDown(keyCode, event);  
    }
	
	private void changeFragmentToCall()
	{ 
		left_icon_panel.select(0);
		CallFragment callFragment = new CallFragment();		
		FragmentTransaction transaction = getFragmentManager().beginTransaction();
		transaction.replace(R.id.fragmentContainer2, callFragment);
		transaction.commit();		
	}
	private void changeFragmentToMap()
	{
		left_icon_panel.select(1);
		WebViewFragment webViewFragment = new WebViewFragment();
		FragmentTransaction transaction = getFragmentManager().beginTransaction();
		transaction.replace(R.id.fragmentContainer2, webViewFragment);
		transaction.commit();
	}
	private void changeFragmentToAudioCall()
	{
		left_icon_panel.select(0);
		AudioCallFragment audioCallFragment = new AudioCallFragment();
		FragmentTransaction transaction = getFragmentManager().beginTransaction();
		transaction.replace(R.id.fragmentContainer2, audioCallFragment);
		transaction.commit();
	}
	private void changeFragmentToVideoCall()
	{
		left_icon_panel.select(0);
		CallVideoFragment videoCallFragment = new CallVideoFragment();
		FragmentTransaction transaction = getFragmentManager().beginTransaction();
		transaction.replace(R.id.fragmentContainer2, videoCallFragment);
		transaction.commit();
	}
	private void changeFragmentToIris()
	{
		left_icon_panel.select(2);
		IrisRecognitionFragment irisFragment = new IrisRecognitionFragment();
		FragmentTransaction transaction = getFragmentManager().beginTransaction();
		transaction.replace(R.id.fragmentContainer2, irisFragment);
		transaction.commit();
	}
	
	
	@Override
	protected void onDestroy(){
		debug.i("onDestroy");		
		super.onDestroy();
	}

	@Override
    protected void onPause()
    {
	    // TODO Auto-generated method stub
		titleBar.onDestroy();
		debug.i("onPause");
	    super.onPause(); 
    }

	@Override
    protected void onResume()
    {
	    // TODO Auto-generated method stub
		debug.i("onResume");
	    super.onResume();
    }

	@Override
    protected void onStart()
    {
	    // TODO Auto-generated method stub
		debug.i("onStart");
	    super.onStart();
    }

	@Override
    public void onClick(View v)
    { 
		debug.i("onClick");
		int id = v.getId();		
		debug.i("id = "+id);
    }
	
}
