package com.sxkeda.launcher.component;

import java.text.SimpleDateFormat;

import org.linphone.R;

import com.sxkeda.launcher.Debug;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class TitleBar extends RelativeLayout
{
	private TextView clock, title;
	private ImageView single, camera;           // 信号图片显示

	private WifiManager wifiManager = null; // Wifi管理器
	private Handler handler, cameraHandler;
	private boolean isCameraBlink = false;
	private Thread cameraThread, wifiRssiThread;
	private boolean cameraThreadFlag, wifiRsisThreadFlag;

	private Debug debug = new Debug(Debug.isTitleBar, "TitleBar");

	public TitleBar(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.title_bar, this);
		clock = (TextView) findViewById(R.id.clock);
		title = (TextView) findViewById(R.id.title);
		single = (ImageView) findViewById(R.id.single);
		camera = (ImageView) findViewById(R.id.camera);
		clock.setText(getSystemDateToString());

		wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		initWifiRssi();
		startWifiRssiThread();

		initCameraBlink();
		startCameraBlink();
	}

	/**
	 * 初始化wifi RSSI 强度变化图标。<br>
	 * 
	 * <pre>
	 * 1.初始化一个handler用于更新UI.
	 * 2.初始化一个thread用户获得wifi信号强度，并且发送handler消息。
	 * 
	 * @author    秦晓宇
	 * @date      2016年7月9日 上午10:11:19
	 */
	private void initWifiRssi()
	{
		if (handler == null)
		{
			handler = new Handler()
			{
				@Override
				public void handleMessage(Message msg)
				{
					debug.i("msg.what = " + msg.what);
					switch(msg.what)
					{
					// 如果收到正确的消息就获取WifiInfo，改变图片并显示信号强度
						case 1:// 信号最好
							single.setImageResource(R.drawable.single5);
							break;
						case 2:// 信号较好
							single.setImageResource(R.drawable.single4);
							break;
						case 3:// 信号一般
							single.setImageResource(R.drawable.single3);
							break;
						case 4:// 信号较差
							single.setImageResource(R.drawable.single2);
							break;
						case 5:// 无信号
							single.setImageResource(R.drawable.single1);
							break;
					}
					// 捎带刷新了显示的时间控件
					clock.setText(getSystemDateToString());
				}
			};
		}

		if (wifiRssiThread == null)
		{
			wifiRssiThread = new Thread(new Runnable()
			{
				@Override
				public void run()
				{
					while (wifiRsisThreadFlag)
					{
						WifiInfo wifiInfo = wifiManager.getConnectionInfo();
						// 获得信号强度值
						int level = wifiInfo.getRssi();
						debug.i("wifi Rssi = " + level);
						// 根据获得的信号强度发送信息
						if (level <= 0 && level >= -50)
						{
							Message msg = new Message();
							msg.what = 1;
							handler.sendMessage(msg);
						}
						else if (level < -50 && level >= -70)
						{
							Message msg = new Message();
							msg.what = 2;
							handler.sendMessage(msg);
						}
						else if (level < -70 && level >= -80)
						{
							Message msg = new Message();
							msg.what = 3;
							handler.sendMessage(msg);
						}
						else if (level < -80 && level >= -100)
						{
							Message msg = new Message();
							msg.what = 4;
							handler.sendMessage(msg);
						}
						else
						{
							Message msg = new Message();
							msg.what = 5;
							handler.sendMessage(msg);
						}

						try
						{
							Thread.sleep(3000);
						}
						catch (InterruptedException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			});
		}
	}

	/**
	 * 停止wifi信号强度ui更新功能
	 * 
	 * @author 秦晓宇
	 * @date 2016年7月9日 上午10:24:23
	 */
	private void stopWifiRssiThread()
	{
		wifiRsisThreadFlag = false;
		if (wifiRssiThread != null)
		{
			wifiRssiThread = null;
		}
	}

	/**
	 * 启动wifi信号强度ui更新功能
	 * 
	 * @author 秦晓宇
	 * @date 2016年7月9日 上午10:24:47
	 */
	private void startWifiRssiThread()
	{
		if (wifiRsisThreadFlag == true) return;

		wifiRsisThreadFlag = true;
		if (wifiRssiThread != null)
		{
			wifiRssiThread.start();
		}
		else
		{
			initWifiRssi();
			wifiRssiThread.start();
		}
	}

	/**
	 * 初始化摄像头图标闪烁功能
	 * 
	 * <pre>
	 * 1.初始化handler更新ui
	 * 2.创建线程负责控制handler刷新时间
	 * @author    秦晓宇
	 * @date      2016年7月9日 上午10:25:04
	 */
	private void initCameraBlink()
	{
		if (cameraHandler == null)
		{
			cameraHandler = new Handler()
			{
				@Override
				public void handleMessage(Message msg)
				{
					debug.i("msg.what = " + msg.what);
					if (isCameraBlink)
					{
						camera.setImageResource(R.drawable.camera_shot);
						isCameraBlink = false;
					}
					else
					{
						camera.setImageResource(R.drawable.camera);
						isCameraBlink = true;
					}
				}
			};
		}
		if (cameraThread == null)
		{
			cameraThread = new Thread(new Runnable()
			{
				@Override
				public void run()
				{
					while (cameraThreadFlag)
					{
						Message msg = new Message();
						msg.what = 1;
						cameraHandler.sendMessage(msg);

						try
						{
							Thread.sleep(1000);
						}
						catch (InterruptedException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

				}
			});
		}

	}

	/**
	 * 停止摄像头闪烁
	 * 
	 * @author 秦晓宇
	 * @date 2016年7月9日 上午10:26:11
	 */
	private void stopCameraBlink()
	{
		cameraThreadFlag = false;
		if (cameraThread != null)
		{
			cameraThread = null;
		}
	}

	/**
	 * 启动摄像头闪烁
	 * 
	 * @author 秦晓宇
	 * @date 2016年7月9日 上午10:26:25
	 */
	private void startCameraBlink()
	{
		if (cameraThreadFlag == true) return;

		cameraThreadFlag = true;
		if (cameraThread != null)
		{
			cameraThread.start();
		}
		else
		{
			initCameraBlink();
			cameraThread.start();
		}
	}

	/**
	 * 设置摄像头图标闪烁与否
	 * 
	 * @author 秦晓宇
	 * @date 2016年7月9日 上午10:26:34
	 * @param enable
	 *            true 闪烁
	 *            false 不闪烁
	 * 
	 */
	public void setCameraBlink(boolean enable)
	{
		if (enable) startCameraBlink();
		else stopCameraBlink();
	}

	/**
	 * 销毁TitleBar控件，主要是停止线程
	 * 
	 * @author 秦晓宇
	 * @date 2016年7月9日 上午10:31:14
	 */
	public void onDestroy()
	{
		stopCameraBlink();
		stopWifiRssiThread();
	}

	/**
	 * 获得系统时间
	 * 
	 * @author 秦晓宇
	 * @date 2016年7月9日 上午10:31:46
	 * @return 系统时间字符串
	 */
	public String getSystemDateToString()
	{
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm");
		String date = simpleDateFormat.format(new java.util.Date());
		return date;
	}

	/**
	 * 设置当前界面名称
	 * 
	 * @author 秦晓宇
	 * @date 2016年7月9日 上午10:32:13
	 * @param title
	 */
	public void setTitle(String title)
	{
		this.title.setText(title);
	}
}
