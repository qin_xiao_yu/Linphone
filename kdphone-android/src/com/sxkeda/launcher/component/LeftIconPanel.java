package com.sxkeda.launcher.component;

import org.linphone.R;

import com.sxkeda.launcher.Debug;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class LeftIconPanel extends RelativeLayout
{

	private ImageView call, map, person, setting, light;
	private int selectIndex = -1;
    ICoallBack icallBack = null;  
	private static final boolean isAnimation = false;
	
	
	public enum ICON_INDEX {CALL, MAP, PERSION, SETTING}
	private Debug debug = new Debug(Debug.isLeftIconPanel, "LeftIconPanel");

	public LeftIconPanel(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.left_icon_list, this);
		call = (ImageView) findViewById(R.id.call);
		map = (ImageView) findViewById(R.id.map);
		person = (ImageView) findViewById(R.id.person);
		setting = (ImageView) findViewById(R.id.setting);
		light = (ImageView) findViewById(R.id.light);
	
	}
    public interface ICoallBack{  
        public void onClickButton(ICON_INDEX index);  
    }  
    public void setonClick(ICoallBack iBack)  
    {  
        icallBack = iBack;  
    }  

	public void select()
	{
		selectIndex++;
		if (selectIndex > 3)
		{
			selectIndex = 0;
		}
		switch(selectIndex)
		{
			case 0:
				setLight(ICON_INDEX.CALL);
			break;
			case 1:
				setLight(ICON_INDEX.MAP);
			break;
			case 2:
				setLight(ICON_INDEX.PERSION);
			break;
			case 3:
				setLight(ICON_INDEX.SETTING);
			break;
		}
	}
	@SuppressWarnings("unused")
    public void select(int index)
	{
		this.selectIndex = index;
		if (isAnimation == true)
		{
			switch(selectIndex)
			{
				case 0:
					animationSet(light, 0, 85);
					break;
				case 1:
					animationSet(light, 85, 170);
					break;
				case 2:
					animationSet(light, 170, 265);
					break;
				case 3:
					animationSet(light, 170, 390);
					break;
			}
		}
		else
		{
			switch(index)
			{
				case 0:
					locationSet(light, 22);					 
					break;
				case 1:
					locationSet(light, 110);					
					break;
				case 2:
					locationSet(light, 220);					
					break;
				case 3:
					locationSet(light, 320);					
					break;
			}
		}
	}
	
	
	
	@SuppressWarnings("unused")
    private void setLight(ICON_INDEX index)
	{
		debug.i("start x = " + light.getX() + "  y = " + light.getY());
		debug.i("call = " + call.getY() + " map = " + map.getY() + " person = " + person.getY() + " setting = " + setting.getY());
		debug.i("selectIndex = "+selectIndex);
		if (isAnimation == true)
		{
			switch(index)
			{
				case CALL:
					animationSet(light, 0, 85);
					icallBack.onClickButton(ICON_INDEX.CALL);  
					break;
				case MAP:
					animationSet(light, 85, 170);
					icallBack.onClickButton(ICON_INDEX.MAP);  
					break;
				case PERSION:
					animationSet(light, 170, 265);
					icallBack.onClickButton(ICON_INDEX.PERSION);  
					break;
				case SETTING:
					animationSet(light, 170, 390);
					icallBack.onClickButton(ICON_INDEX.SETTING);  
					break;
			}
		}
		else
		{
			switch(index)
			{
				case CALL:
					icallBack.onClickButton(ICON_INDEX.CALL); 
					locationSet(light, 22);					 
					break;
				case MAP:
					icallBack.onClickButton(ICON_INDEX.MAP); 
					locationSet(light, 110);					
					break;
				case PERSION:
					icallBack.onClickButton(ICON_INDEX.PERSION); 
					locationSet(light, 220);					
					break;
				case SETTING:
					icallBack.onClickButton(ICON_INDEX.SETTING); 
					locationSet(light, 320);					
					break;
			}
		}
	}

    private void locationSet(View view, int y)
	{
		MarginLayoutParams margin = new MarginLayoutParams(view.getLayoutParams());
		margin.setMargins(margin.leftMargin, y, margin.rightMargin, margin.bottomMargin);
		RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(margin);
		view.setLayoutParams(layoutParams);
	}

	private void animationSet(View view, int starty, int endy)
	{
		TranslateAnimation animation = new TranslateAnimation(0, 0, starty, endy);
		animation.setInterpolator(new LinearInterpolator());
		animation.setDuration(500);// 设置动画持续时间
		animation.setFillAfter(true);
		view.startAnimation(animation);
	}
}
