package com.sxkeda.launcher;

import android.util.Log;

public class Debug {
	
	
	
	public static final boolean isTitleBar = false;
	public static final boolean isVideoCallFragment = true;
	public static final boolean isKedaMapFragment = true;
	public static final boolean isKedaCallFragment = true;
	public static final boolean isLeftIconPanel = false;
	public static final boolean isMainActivity = true;
	public static final boolean isIrisRecognitionActivity = true;
	public static final boolean isKedaIrisRecognitionFragment = true;
	public static final boolean isAVselectDialg = true;
	public static final boolean isAudioCallFragment = true;
	
	
	
	
	
	
	
	
	
	private String tag;
	private boolean isDebug;

	
	public Debug(boolean isDebug,String tag)
	{
		this.isDebug = isDebug;
		this.tag = tag;		

	}

	public void i(String str)
	{
		if(isDebug == true && tag!=null)
		Log.i(tag, str);		
	}
	public void w(String str)
	{
		if(isDebug == true && tag!=null)
		Log.w(tag, str);		
	}
	public void d(String str)
	{
		if(isDebug == true && tag!=null)
		Log.d(tag, str);		
	}
	public void e(String str)
	{
		if(isDebug == true && tag!=null)
		Log.e(tag, str);		
	}

}
