package com.sxkeda.launcher;

import java.util.ArrayList;
import java.util.List;

import org.linphone.R;
import org.linphone.core.LinphoneCore;

import com.bohua.HDJni.JniUart;
import com.sxkeda.cctv.CCTV;
import com.sxkeda.isirRecognition.IsirRecognition;
import com.sxkeda.launcher.component.TitleBar;
import com.sxkeda.voip.Business;
import com.sxkeda.voip.event.Operater;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;

public class IrisRecognitionActivity extends Activity
{
	
	private Debug debug = new Debug(Debug.isIrisRecognitionActivity, "IrisRecognitionActivity");
	private TitleBar titleBar;
	private Business kedaBusiness;
	private Operater operater;
	private CCTV cctv;
	IsirRecognition isirRecognition;
	Handler mhandler;
	Isir isir;
	@Override
    protected void onCreate(Bundle savedInstanceState)
    { 
	    // TODO Auto-generated method stub
	    super.onCreate(savedInstanceState);	  
	    this.requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);//去掉信息栏
		setContentView(R.layout.iris_recognition_login);
		titleBar = (TitleBar)findViewById(R.id.titleBar);
		titleBar.setTitle("人员识别");
		operater = new Operater();
		kedaBusiness = new Business(getApplicationContext());
		cctv = new CCTV(getApplicationContext());
		cctv.startCctvService();

		mhandler = new Handler()
		{
			@Override
			public void handleMessage(Message msg)
			{
				debug.i("msg.what = " + msg.what);
				switch(msg.what)
				{
					case 1:
						xxx();
					break;
				}
			}
		};
		
		isirRecognition = new IsirRecognition();
		isirRecognition.start();
		
		isir = new Isir();
		isir.start();
		
    }
	private void xxx(){
		
		if(isir != null ) isir.setCancel();
		if(isirRecognition!=null) isirRecognition.onDistry();
		startActivity(new Intent().setClass(this, MainActivity.class));
		finish();
	}
	
	@Override
    protected void onDestroy()
    {
	    // TODO Auto-generated method stub
		JniUart.close();
		if(isir != null ) isir.setCancel();
	    super.onDestroy();
    }

	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
		debug.i("keyCode = "+keyCode);
		switch(keyCode)  
	    {  
			case KeyEvent.KEYCODE_HOME:
				return true;
			case KeyEvent.KEYCODE_BACK:
				break;
			case KeyEvent.KEYCODE_CALL:
				this.finish();
			case KeyEvent.KEYCODE_SYM:
				return true;
			case KeyEvent.KEYCODE_VOLUME_DOWN:
			{
				Message msg = new Message();
				msg.what = 1;
				mhandler.sendMessage(msg);
			}
				return true;
			case 82:
			{
				isirRecognition.senCmd();

			}
				return true;
			case KeyEvent.KEYCODE_VOLUME_UP:
				return true;
			case KeyEvent.KEYCODE_STAR:
				return true;
		}
	    return super.onKeyDown(keyCode, event);  
    }
	
	public class Isir extends Thread
	{
		boolean isRun = true;
		
		public synchronized void setCancel(){
			isRun = false;
		}
		
		@Override
		public void run()
		{
			while (isRun)
			{
				if(isirRecognition.uartRcvThrad.isAllow())
				{
					Message msg = new Message();
					msg.what = 1;
					mhandler.sendMessage(msg);
				}
			}
		}
	}
	

	
	
	
}
