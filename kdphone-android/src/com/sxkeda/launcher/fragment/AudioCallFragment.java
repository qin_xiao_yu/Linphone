package com.sxkeda.launcher.fragment;

import org.linphone.R;
import org.linphone.core.LinphoneCall;

import com.sxkeda.launcher.Debug;
import com.sxkeda.voip.Business;
import com.sxkeda.voip.CallApi;
import com.sxkeda.voip.Business.CallInformation;
import com.sxkeda.voip.event.Listener;
import com.sxkeda.voip.event.Operater;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class AudioCallFragment extends Fragment
{	
	private Debug debug = new Debug(Debug.isAudioCallFragment, "AudioCallFragment");
	
	private Listener mListener;
	private TextView nameTextView,timeTextView;
	private Button hangup;
	
	private static AudioCallFragment instance;
	
	public static synchronized final AudioCallFragment instance() {
		if (instance != null) return instance;		
		throw new RuntimeException("Operater should be created before accessed");
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
	    // TODO Auto-generated method stub
		debug.i("onCreateView");
		View view = inflater.inflate(R.layout.audio_call_fragment, container, false);
		nameTextView = (TextView)view.findViewById(R.id.name);
		timeTextView = (TextView)view.findViewById(R.id.time);		
		hangup = (Button)view.findViewById(R.id.hangup); 

		nameTextView.setText(Business.getInstance().getCallName());
		
		initListener();
		setViewFocus(hangup);
		return view;
    }
	
	private void initListener()
	{
		mListener = new Listener()
		{

			@Override
			public void updateRegationStatus(String string){}
			@Override
			public void updataLinphoneServiceStatus(boolean status){}
			@Override
			public void updataCallTime(String time)
			{
				timeTextView.setText(time);
			}
			@Override
			public void updataCallStatus(LinphoneCall.State state){}
			@Override
			public void updataCallNumber(String number)
			{
			}
			@Override
            public void updataCallInformation(CallInformation callInformation){}

		};
		Operater.getInstance().addListener(mListener);
	}
	public void click()
	{
		debug.i("click");
		CallApi.hangUp();
	}
	private void setViewFocus(View view)
	{
		view.setFocusable(true);
		view.setFocusableInTouchMode(true);
		view.requestFocus();
	}
	@Override
	public void onDestroy()
	{
		// TODO Auto-generated method stub
		super.onDestroy();
	}
}
