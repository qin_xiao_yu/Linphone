package com.sxkeda.launcher.fragment;

import org.linphone.R;

import com.sxkeda.launcher.Debug;
import com.sxkeda.mAppwidget.RoadWayMapWidget;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class MapFragment extends Fragment
{
	private Debug debug = new Debug(Debug.isKedaMapFragment, "KedaMapFragment");

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		debug.i("onCreateView");
		View view = inflater.inflate(R.layout.keda_map_fragment, container, false);

		final RoadWayMapWidget map = new RoadWayMapWidget(MapFragment.this.getActivity(), "map",15);
		LinearLayout layout = (LinearLayout) view.findViewById(R.id.mainLayout);		
		map.getConfig().setZoomBtnsVisible(false);
		layout.addView(map);		
		
		map.moveTo(10000, 100,150000);
		
		return view;
	}

	@Override
	public void onDestroy()
	{
		// TODO Auto-generated method stub
		debug.i("onDestroy");
		super.onDestroy();
	}

	@Override
	public void onResume()
	{
		// TODO Auto-generated method stub
		debug.i("onResume");
		super.onResume();
	}

}
