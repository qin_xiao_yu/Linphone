package com.sxkeda.launcher.fragment;

import org.linphone.R;
import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCall.State;

import com.sxkeda.launcher.Debug;
import com.sxkeda.launcher.component.AVselectDialg;
import com.sxkeda.voip.Business;
import com.sxkeda.voip.Business.CallInformation;
import com.sxkeda.voip.CallApi;
import com.sxkeda.voip.event.Listener;
import com.sxkeda.voip.event.Operater;

import android.app.AlertDialog;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

public class CallFragment extends Fragment
{
	private Debug debug = new Debug(Debug.isKedaCallFragment, "KedaCallFragment");
	private static CallFragment instance;
	private Listener mListener;
	private TextView registeState, callnumber, callStateView;
	private Button call;
	private ToggleButton avRecive;
	private String regationState/*,callState*/;
	private int foucsFlag = 1;
	LinphoneCall.State callState;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		debug.i("onCreateView");
		View view = inflater.inflate(R.layout.keda_call_fragment, container, false);
		instance = this;
		initListener();
		initViews(view);

		
		regationState = Business.getInstance().getRegistrationState().toString();
		registeState.setText(/*changeRegistrationChina(regationState)*/regationState);	
		
		callState = Business.getInstance().getCallStatus();
//		if(callState == null)	callStateView.setText("");
//		else					callStateView.setText(callState.toString());
		
		setFocus();
		return view;
	}
	public static synchronized final CallFragment instance()
	{
		if (instance != null) return instance;
		throw new RuntimeException("KedaBusiness should be created before accessed");
	}
	private void initViews(View view)
	{
		registeState = (TextView) view.findViewById(R.id.registeState);
		callnumber = (TextView) view.findViewById(R.id.callnumber);
		callStateView = (TextView) view.findViewById(R.id.callState);

		call = (Button) view.findViewById(R.id.call);
//		avRecive = (ToggleButton) view.findViewById(R.id.avRecive);
//		avRecive.setOnCheckedChangeListener(new ToggleButton.OnCheckedChangeListener()
//		{
//			@Override
//			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
//			{
//				// TODO Auto-generated method stub
//				if (isChecked)
//				{
//					Business.getInstance().setIsAllowVideoOrAudioCall(true);
//				}
//				else
//				{
//					Business.getInstance().setIsAllowVideoOrAudioCall(false);
//				}
//			}
//		});
		call.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				debug.i("call setOnClickListener");
				// TODO Auto-generated method stub
				if(callState == null) return;
				debug.i("callState = "+callState.toString());
				if (callState == State.StreamsRunning || callState == State.OutgoingEarlyMedia||callState == State.OutgoingRinging)
				{
					debug.i("hangUp");
					CallApi.hangUp();
				}
				else if (callState == State.CallEnd || callState == State.Error || callState == State.CallReleased)
				{
					debug.i("dial");
					Business.getInstance().newAudioCall("1006");
				}									
			}
		});
	}

	private void initListener()
	{
		mListener = new Listener()
		{ 

			@Override
			public void updateRegationStatus(String string)
			{
				debug.i("updateRegationStatus("+string+")");
//				regationState = string;
				registeState.setText(string);
			}

			@Override
			public void updataLinphoneServiceStatus(boolean status)
			{
				debug.i("updataLinphoneServiceStatus(" + status + ")");
			}

			@Override
			public void updataCallTime(String time)
			{
				// debug.i("updataCallTime("+time+")");
			}

			@Override
			public void updataCallStatus(LinphoneCall.State state)
			{
				// debug.i("updataCallStatus("+state.toString()+")");
				callState = state;
//				callStateView.setText(state.toString());
				if (state == State.StreamsRunning || state == State.OutgoingEarlyMedia||state == State.OutgoingRinging)
				{
					call.setText("�Ҷ�");
				}
				else if (state == State.CallEnd || state == State.Error || state == State.CallReleased)
				{
					call.setText("����");
				}				
			}

			@Override
			public void updataCallNumber(String number)
			{
				// debug.i("updataCallNumber("+number+")");
				callnumber.setText(number);
			}

			@Override
			public void updataCallInformation(CallInformation callInformation)
			{
				// debug.i("updataCallInformation()");
			}
		};
		Operater.getInstance().addListener(mListener);
	}

	public void setFocusNext()
	{
		debug.i("setFocusNext");
		debug.i("foucsFlag 1 = " + foucsFlag);
		foucsFlag++;
		if (foucsFlag > 1) foucsFlag = 1;
		setFocus();
	}

	public void setFocusPrevious()
	{
		debug.i("setFocusPrevious");
		debug.i("foucsFlag 1 = " + foucsFlag);
		foucsFlag--;
		if (foucsFlag < 1) foucsFlag = 1;
		setFocus();
	}

	private void setFocus()
	{
		debug.i("foucsFlag = " + foucsFlag);
		if (foucsFlag == 1)
		{
			setViewFocus(call);
		}
//		else if (foucsFlag == 2)
//		{
//			setViewFocus(avRecive);
//		}
	}

	private void setViewFocus(View view)
	{
		view.setFocusable(true);
		view.setFocusableInTouchMode(true);
		view.requestFocus();
	}

	public void click()
	{
		debug.i("click");
		if (foucsFlag == 1)
		{
			callState = Business.getInstance().getCallStatus();

			if(callState == null)
			{
				debug.i("dial");
				//KedaBusiness.getInstance().newAudioCall("1006");
				dailTypeSelect();
				return;
			}
			
			if (callState == State.StreamsRunning || callState == State.OutgoingEarlyMedia||callState == State.OutgoingRinging)
			{
				debug.i("hangUp");
				CallApi.hangUp();
			}
			else if (callState == State.CallEnd || callState == State.Error || callState == State.CallReleased)
			{
				debug.i("dial");
				//KedaBusiness.getInstance().newAudioCall("1006");
				dailTypeSelect();
			}	
		}
		else if (foucsFlag == 2)
		{
			if (avRecive.isChecked()) avRecive.setChecked(false);
			else
			{
				avRecive.setChecked(true);
			}
		}
	}
	private void dailTypeSelect()
	{
		AVselectDialg aVselectDialg = new AVselectDialg(CallFragment.this.getActivity());
		aVselectDialg.show();
	}
	@Override
	public void onDestroy()
	{
		// TODO Auto-generated method stub
		super.onDestroy();
	}
	public String changeRegistrationChina(String state){
		if(state.equals("Registration successful")) return "ע��ɹ�";
		else return "ע��ʧ��";
	}

}
