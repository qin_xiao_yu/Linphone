package com.sxkeda.launcher.fragment;
 
import org.linphone.R;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebViewFragment extends Fragment
{ 
	private void debug(String str)
	{
		Log.i("WebView", str);
	}       
              
	@Override   
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{            
		// TODO Auto-generated method stub  
		debug("onCreateView");
		View view = inflater.inflate(R.layout.webview, container, false);
		String baidumap = "http://map.baidu.com/mobile/webapp/index/index";
		String baidu = "http://192.168.1.33:8011/";
		String openLayer = "http://192.168.1.11:8080/geoserver/tiger/wms?service=WMS&version=1.1.0&request=GetMap&layers=tiger:tiger_roads&styles=&bbox=-74.02722,40.684221,-73.907005,40.878178&width=476&height=768&srs=EPSG:4326&format=application/openlayers";
		String openLayer1 = "http://192.168.1.11:8080/geoserver/web/wicket/bookmarkable/org.geoserver.web.demo.MapPreviewPage?2";
		String flexlayer = "http://openscales.org/"; 
		// String qxy =  
		// Environment.getExternalStorageDirectory().getPath()+"/privateOpenlayer_IndoorMap/sxkeda/main.html";
//		 String qxy = "file:///android_asset/sxkeda/main.html";
		String qxy = "file:///android_asset/index.html";
//		String qxy = "http://192.168.0.102:8060/privateOpenlayer_IndoorMap/sxkeda/main.html";
//		String qxy = "https://192.168.0.102:8061/privateOpenlayer_IndoorMap/sxkeda/main.html";
//		String gyy = "http://192.168.1.33:8011/openlayer_test";
		debug(qxy);  
		WebView webView = (WebView) view.findViewById(R.id.webView1);
		webView.setWebChromeClient(new WebChromeClient()
		{ 
			public void onConsoleMessage(String message, int lineNumber, String sourceID) 
			{  
				// Log.d("MyApplication", message + " -- From line "
				// + lineNumber + " of " 
				// + sourceID); 
			}     
		});   
		webView.getSettings().setJavaScriptEnabled(true); 
		// webView.getSettings().setDomStorageEnabled(true);
		// webView.getSettings().setBlockNetworkImage(true); 
		// webView.getSettings().setBlockNetworkLoads(true);
//		webView.getSettings().setBuiltInZoomControls(true);
//		webView.getSettings().setDatabaseEnabled(true);
//		webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
//		webView.getSettings().setLoadsImagesAutomatically(true);
//		webView.getSettings().setUseWideViewPort(true);
//		webView.getSettings().setSupportMultipleWindows(true);
		// webView.getSettings().setAllowFileAccessFromFileURLs(true);
		// webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
//		webView.getSettings().setAllowFileAccess(true);
//		webView.getSettings().setAllowContentAccess(true);
//		webView.getSettings().setAppCacheEnabled(true);   
		webView.loadUrl(qxy);  
		// WebSettings settings = webView.getSettings(); 
		// settings.setJavaScriptEnabled(true);
   
		// 覆盖WebView默认使用第三方或系统默认浏览器打开网页的行为，使网页用WebView打开
		// webView.setWebViewClient(new WebViewClient()
		// {
		// @Override
		// public boolean shouldOverrideUrlLoading(WebView view, String url)
		// {
		// // TODO Auto-generated method stub
		// // 返回值是true的时候控制去WebView打开，为false调用系统浏览器或第三方浏览器
		// view.loadUrl(url);
		// return true;
		// }
		// });
		// webView.setWebChromeClient(new WebChromeClient() {
		// public boolean onConsoleMessage(ConsoleMessage cm) {
		// Log.d("MyApplication", cm.message() + " -- From line "
		// + cm.lineNumber() + " of "
		// + cm.sourceId() );
		// return true;
		// }
		// });

		return view;
	}

}
