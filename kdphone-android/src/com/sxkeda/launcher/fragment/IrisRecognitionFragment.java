package com.sxkeda.launcher.fragment;

import org.linphone.R;

import com.sxkeda.launcher.Debug;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class IrisRecognitionFragment extends Fragment
{
	private Debug debug = new Debug(Debug.isKedaIrisRecognitionFragment, "KedaIrisRecognitionFragment");
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
		debug.i("onCreateView");
		View view = inflater.inflate(R.layout.keda_iris_recognition_fragment, container, false);
		return view;
    }

}
