package com.sxkeda.isirRecognition;

import java.util.ArrayList;
import java.util.List;

import com.bohua.HDJni.JniUart;
import com.sxkeda.launcher.Debug;

public class IsirRecognition extends Thread
{

	private static boolean isUartRcv = true;
	private static boolean isUartSend = true;
	private static boolean isIsirRecognitionThread = true;
	
	public UartRcvThrad uartRcvThrad;
	UartSendThread uartSendThread;
	private boolean isirSendCmd = true;
	
	
	static char[] init = {0x55 ,0x00 ,0x0A ,0x00 ,0x00 ,0x00 ,0x00 ,0x01 ,0x41 ,0x00 ,0x00 ,0xE0 ,0x03};
	static char[] modeSwitch = {0x55 ,0x00 ,0x0B ,0x00 ,0x00 ,0x00 ,0x00 ,0x01 ,0x4B ,0x00 ,0x01 ,0x02 ,0xE8 ,0x03};
	static char[] recognition = {0x55 ,0x00 ,0x22 ,0x00 ,0x00 ,0x00 ,0x00 ,0x01 ,0x42 ,0x00 ,0x18 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00
		,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00 ,0xD3 ,0x03};
	
	private Debug debug = new Debug(false, "IsirRecognition");
	
	
	public IsirRecognition()
	{
		uartRcvThrad = new UartRcvThrad();
		if(JniUart.jniSetUartPara() == -1)
		{
			onDistry();
			return;	
		}
		uartRcvThrad.start();
//		uartSendThread = new UartSendThread();
//		uartSendThread.start();	
	}
	public synchronized void senCmd(){
		isirSendCmd = true;
	}

	@Override
    public void run()
    {		
		try
        {
            Thread.sleep(1000);
            JniUart.initISIR();
        }
        catch (InterruptedException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
		try
        {
            Thread.sleep(1000);
            JniUart.modeSwitch();
        }
        catch (InterruptedException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
		
		while(isIsirRecognitionThread)
		{
			try
            {
	            Thread.sleep(300);
            }
            catch (InterruptedException e)
            {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
            }
			if(isirSendCmd)
			{
		        JniUart.recognition();
		        isirSendCmd = false;
			}
		}
    }



	public void onDistry()
	{
		isIsirRecognitionThread = false;
		isUartRcv = false;
		isUartSend = false;
	}

	public class UartRcvThrad extends Thread
	{
		private List<String> uartRcv;
		boolean isIsir = false;

		public UartRcvThrad() 
		{
			uartRcv = new ArrayList<String>();
		} 

		public synchronized String getUartRcv()
		{
			if (uartRcv.size() > 0)
			{
				return uartRcv.get(0);
			}
			return null;
		}

		private synchronized void setUartRcv(String uartRcv)
		{
			this.uartRcv.add(uartRcv);
		}
		public synchronized boolean isAllow()
		{
			boolean tmp = isIsir;
			isIsir = false;
			return tmp;
		}
		public synchronized void setCancel()
		{
			isUartRcv = false;
		}
		
		
		
		@Override
		public void run()
		{
			while (isUartRcv)
			{
				String data = JniUart.read();
				debug.i("data = "+data);
				if (data != null && !data.equals(""))
				{
					debug.i("data len = "+data.length());
					debug.i("xxx = "+ toHexString(new String(data)));
					debug.i("uart rcv = "+data);
//					setUartRcv(data);
					if(data.equals("1"))
					{
						isIsir = true;
						isUartRcv = false;
					}
				}
			}
		}
		
	}

	public class UartSendThread extends Thread
	{
		private List<String> uartSend;

		public UartSendThread()
		{
			uartSend = new ArrayList<String>();
		}

		public synchronized void setUartSend(String uartSend)
		{
			this.uartSend.add(uartSend);
		}

		private synchronized void sendUartData()
		{
			if (uartSend.size() > 0)
			{
				String data = uartSend.get(0);
				debug.i("uart write = "+data);
//				JniUart.write(data);
				uartSend.remove(0);
			}
		}

		@Override
		public void run()
		{
			while (isUartSend)
			{
				sendUartData();
			}
		}
	}
	/**  
	 * bytes转换成十六进制字符串  
	 * @param byte[] b byte数组  
	 * @return String 每个Byte值之间空格分隔  
	 */    
	public static String byte2HexStr(byte[] b)    
	{    
	    String stmp="";    
	    StringBuilder sb = new StringBuilder("");    
	    for (int n=0;n<b.length;n++)    
	    {    
	        stmp = Integer.toHexString(b[n] & 0xFF);    
	        sb.append((stmp.length()==1)? "0"+stmp : stmp);    
	        sb.append(" ");    
	    }    
	    return sb.toString().toUpperCase().trim();    
	}    
	public static String toHexString(String s) {
		String str = "";
		for (int i = 0; i < s.length(); i++) {
			int ch = (int) s.charAt(i);
			String s4 = Integer.toHexString(ch)+" ";
			str = str + s4;
		}
		return str;
	}
}
