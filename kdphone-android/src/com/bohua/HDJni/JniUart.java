package com.bohua.HDJni;

import android.util.Log;

public class JniUart {

	public static int init_state = -1;
	public static String read()
	{
		return jniReadUart();
	}

	public static int write(String str,int len)
	{
		Log.i("JniUart","init_state = "+init_state);
		if(init_state == -1) return init_state;
		return jniWriteUart(str,len);
	}

	public static int init(String baud)
	{
		init_state = jniSetUartPara();
		return init_state;
	}

	 
	
	/*** JNI ***/
	/*初始化串口：：波特率等*/
	public native static int jniSetUartPara();
    /*写串口数据*/
    public native static int jniWriteUart(String data,int len);
    /*读串口数据*/
	public native static String jniReadUart();
	public native static int recognition();
	public native static int modeSwitch();
	public native static int initISIR();
	public native static int close();
    static {
        System.loadLibrary("uart");
    }
	
}
