package com.bohua.data.rs485;

import java.io.File;

import android.util.Log;

import com.bohua.base.FileProperties;

public class Rs485Data {
	
	public static String ipAddr;				//本地IP
	public static String netMask;				//子网掩码
	public static String rs485Buad;				//485波特率
	public static String rs485bits;				//485数据位
	public static String rs485stopBit;			//485停止位
	public static String rs485check;			//485校验位
	public static String rs485Flow;				//485流控
	public static String net485Ip;				//485网络IP
	public static int net485port;				//485网络端口
	public static String snId;						//设备SN id
	
	protected static String path = "/sdcard/switchboard.cfg";
	public static void readAllConfigData()
	{
		ipAddr = FileProperties.readProperties(path, "ipAddr");
		netMask = FileProperties.readProperties(path, "netMask");
		rs485Buad = FileProperties.readProperties(path, "rs485Buad");
		rs485bits = FileProperties.readProperties(path, "rs485bits");
		rs485stopBit = FileProperties.readProperties(path, "rs485stopBit");
		rs485check = FileProperties.readProperties(path, "rs485check");
		rs485Flow = FileProperties.readProperties(path, "rs485Flow");
		net485Ip = FileProperties.readProperties(path, "net485Ip");
		String rs485port = FileProperties.readProperties(path, "net485port");
		net485port = Integer.parseInt(rs485port);
		snId = FileProperties.readProperties(path, "snId");
	}
	
	public static String readOneLine(String key)
	{
		return FileProperties.readProperties(path, key);
	}
	public static void writeOneLine(String parameterName,String parameterValue)
	{
		FileProperties.writeProperties(path, parameterName, parameterValue);
	}
	public static void loadDefaultParam()
	{
		File file = new File(path);
    	if(!file.exists())
    	{
    		Log.d("Rs485Data","485配置文件不存在，创建和填入初始值");
    		creatDefaultConfigFile();
    	}
		Log.d("Rs485Data","485配置文件已经存在，不需要创建和填入初始值");
	}
	public static void creatDefaultConfigFile()	//创建配置文件
	{
		FileProperties.writeProperties(path, "ipAddr", "192.163.20.212");
		FileProperties.writeProperties(path, "netMask", "255.255.255.0");
		FileProperties.writeProperties(path, "rs485Buad", "115200");
		FileProperties.writeProperties(path, "rs485bits", "8");
		FileProperties.writeProperties(path, "rs485stopBit", "1");
		FileProperties.writeProperties(path, "rs485check", "None");
		FileProperties.writeProperties(path, "rs485Flow", "None");
		FileProperties.writeProperties(path, "net485Ip", "192.163.20.14");
		FileProperties.writeProperties(path, "net485port", "3344");
		int snId = (int) (Math.random() * 999999999); 
		FileProperties.writeProperties(path, "snId", snId+"");
	}
	
	
}
