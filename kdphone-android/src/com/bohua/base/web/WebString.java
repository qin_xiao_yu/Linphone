package com.bohua.base.web;

import com.bohua.HDJni.Jni485;
import com.bohua.HDJni.JniUart;
import com.bohua.base.FileHandler;
import com.bohua.base.RootCmd;
import com.bohua.base.thread.RS485ClientSendThread;
import com.bohua.data.rs485.Rs485Data;

import android.util.Log;

public class WebString {

	public static String setString(		//输出Html数据
			String ipAddr,				//本地IP
			String netMask,				//子网掩码
			String rs485Buad,			//485波特率
			String rs485bits,			//485数据位
			String rs485stopBit,		//485停止位
			String rs485check,			//485校验位
			String rs485Flow,			//485流控
			String net485Ip,			//485网络IP
			String net485port,			//485网络端口
			String boot)				//是否复位设备
	{
		String ret = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"
				+ "<html xmlns=\"http://www.w3.org/1999/xhtml\">"
				+"<head>"
				+"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />"
				+"<title>千兆交换机485转网络设置</title>"
				+"</head>"
				+"<form action=\"\" method=\"get\">"
				+"  <p>本地网络配置</p>"
				+"  <p style=\"margin-left:30px\">本地网络: "
				+"  	<input type=\"text\" name=\"local_ipaddr\" style=\"width:120px\""
				+" value=\""+ipAddr+"\""
				+ "/>"
				+"  </p>" 
				+"  <p style=\"margin-left:30px\">子网掩码: "
				+"  	<input type=\"text\" name=\"local_netmask\" style=\"width:120px\""
				+" value=\""+netMask+"\""
				+ "/>"
				+"  </p>"
				+"  <p>485设置</p>"
				+selectedUartBaud(rs485Buad)
				+selectedUartDatabits(rs485bits)
				+selectedUartStopbit(rs485stopBit)
				+selectedUartCheckbit(rs485check)
				+selectedUartFlow(rs485Flow)
				+set485NetIp(net485Ip)
				+set485NetPort(net485port)
				+"<p>设备属性</p>"
				+"<p style=\"margin-left:30px\">设备ID  :"+Rs485Data.snId+"</p>"
				+selectedIfReboot(boot)
				+"  <input type=\"submit\" value=\"提交\" />"
				+"<body>"
				+"</body>"
				+"</html>"
				;

		return ret;
	}
	protected static String set485NetIp(String ip)
	{
		String head = "  <p style=\"margin-left:30px\">485网络中转IP: "
					+"  	<input type=\"text\" name=\"net485_ip\" style=\"width:96px\"";
		String body = " value=\""+ip+"\"";
		String end  = "/>"
					+"  </p>";
		return head+body+end;
	}
	protected static String set485NetPort(String port)
	{
		String head = "  <p style=\"margin-left:30px\">485网络中转端口: "
					+"  	<input type=\"text\" name=\"net485_port\" style=\"width:80px\"";
		String body = " value=\""+port+"\"";
		String end  = "/>"
					+"  </p>";
		return head+body+end;
	}
	private static String selectedUartBaud(String baud)
	{
		String head = "  <p style=\"margin-left:30px\">波特率"
					 +"  	<select name=\"485_buad\" style=\"width:85px\">";
		String body = null;
		if(baud.equals("115200"))	body+="    	<option value=\"115200\" selected=\"selected\">115200</option>";
		else						body+="    	<option value=\"115200\">115200</option>";
		
		if(baud.equals("57600"))	body+="    	<option value=\"57600\" selected=\"selected\">57600</option>";
		else						body+="    	<option value=\"57600\">57600</option>";
			
		if(baud.equals("38400"))	body+="    	<option value=\"38400\" selected=\"selected\">38400</option>";
		else						body+="    	<option value=\"38400\">38400</option>";
			
		if(baud.equals("19200"))	body+="    	<option value=\"19200\" selected=\"selected\">19200</option>";
		else						body+="    	<option value=\"19200\">19200</option>";
			
		if(baud.equals("9600"))		body+="    	<option value=\"9600\" selected=\"selected\">9600</option>";
		else						body+="    	<option value=\"9600\">9600</option>";
		
		String end = "    </select>"
					+"  </p>	";
		return head+body+end;
	}
	private static String selectedUartDatabits(String bits)
	{ 
		String head = "  <p style=\"margin-left:30px\">数据位"
					 +"  	<select name=\"485_bit\" style=\"width:85px\">";
		String body = null;
		if(bits.equals("8"))	body+="    	<option value=\"8\" selected=\"selected\">8</option>";
		else					body+="    	<option value=\"8\">8</option>";
		
		if(bits.equals("7"))	body+="    	<option value=\"7\" selected=\"selected\">7</option>";
		else					body+="    	<option value=\"7\">7</option>";
			
		if(bits.equals("6"))	body+="    	<option value=\"6\" selected=\"selected\">6</option>";
		else					body+="    	<option value=\"6\">6</option>";
			
		if(bits.equals("5"))	body+="    	<option value=\"5\" selected=\"selected\">5</option>";
		else					body+="    	<option value=\"5\">5</option>";
		
		String end = "    </select>"
					+"  </p>	";
		return head+body+end;
	}
	private static String selectedUartStopbit(String bits)
	{
		String head = "  <p style=\"margin-left:30px\">停止位"
					 +"  	<select name=\"485_stopbit\" style=\"width:85px\">";
		String body = null;
		if(bits.equals("1"))	body+="    	<option value=\"1\" selected=\"selected\">1</option>";
		else					body+="    	<option value=\"1\">1</option>";
		
		if(bits.equals("1.5"))	body+="    	<option value=\"1.5\" selected=\"selected\">1.5</option>";
		else					body+="    	<option value=\"1.5\">1.5</option>";
			
		if(bits.equals("2"))	body+="    	<option value=\"2\" selected=\"selected\">2</option>";
		else					body+="    	<option value=\"2\">2</option>";
		
		String end = "    </select>"
					+"  </p>	";
		return head+body+end;
	}
	private static String selectedUartCheckbit(String bit)
	{
		String head = "  <p style=\"margin-left:30px\">校验位"
					 +"  	<select name=\"485_checkbit\" style=\"width:85px\">";
		String body = null;
		if(bit.equals("None"))		body+="    	<option value=\"None\" selected=\"selected\">None</option>";
		else						body+="    	<option value=\"None\">None</option>";
		
		if(bit.equals("Odd"))		body+="    	<option value=\"Odd\" selected=\"selected\">Odd</option>";
		else						body+="    	<option value=\"Odd\">Odd</option>";
			
		if(bit.equals("Even"))		body+="    	<option value=\"Even\" selected=\"selected\">Even</option>";
		else						body+="    	<option value=\"Even\">Even</option>";
			
		if(bit.equals("Mark"))		body+="    	<option value=\"Mark\" selected=\"selected\">Mark</option>";
		else						body+="    	<option value=\"Mark\">Mark</option>";
			
		if(bit.equals("Space"))		body+="    	<option value=\"Space\" selected=\"selected\">Space</option>";
		else						body+="    	<option value=\"Space\">Space</option>";
		
		String end = "    </select>"
					+"  </p>	";
		return head+body+end;
	}
	private static String selectedUartFlow(String baud)
	{
		String head = "  <p style=\"margin-left:30px\">流控制"
					 +"  	<select name=\"485_flow\" style=\"width:85px\">";
		String body = null;
		if(baud.equals("None"))	body+="    	<option value=\"None\" selected=\"selected\">None</option>";
		else						body+="    	<option value=\"None\">None</option>";
		
		if(baud.equals("Hardware"))	body+="    	<option value=\"Hardware\" selected=\"selected\">Hardware</option>";
		else						body+="    	<option value=\"Hardware\">Hardware</option>";
			
		if(baud.equals("Software"))	body+="    	<option value=\"Software\" selected=\"selected\">Software</option>";
		else						body+="    	<option value=\"Software\">Software</option>";
			
		if(baud.equals("Custom"))	body+="    	<option value=\"Custom\" selected=\"selected\">Custom</option>";
		else						body+="    	<option value=\"Custom\">Custom</option>";
		
		String end = "    </select>"
					+"  </p>	";
		return head+body+end;
	}
	private static String selectedIfReboot(String if_boot)
	{
		String head = "  <p style=\"margin-left:30px\">复位转换器"
					 +"  	<select name=\"reboot_485\" style=\"width:85px\">";
		String body = null;
		if(if_boot.equals("None"))		body+="    	<option value=\"None\" selected=\"selected\">None</option>";
		else							body+="    	<option value=\"None\">None</option>";
		
		if(if_boot.equals("Reboot"))	body+="    	<option value=\"Reboot\" selected=\"selected\">Reboot</option>";
		else							body+="    	<option value=\"Reboot\">Reboot</option>";
		
		String end = "    </select>"
					+"  </p>	";
		return head+body+end;
	}
	public static String getRequestIPaddr(String start_str,String end_str,String request)
	{
		if(request.indexOf(start_str+"=") == -1) return null;
		int start = request.indexOf(start_str+"=")+start_str.length()+"=".length();
		int end = request.indexOf(end_str,start);
//		Log.i("WebString","start="+start);
//		Log.i("WebString","end="+end);
		if(start==-1||end==-1)
			return null;
		if(start>end)
			return null;
		else
			return request.substring(start, end);
	}
	
	public static String local_ipaddr = null;
	public static String local_netmask = null;
	public static String rs485_buad = null;
	public static String rs485_bit = null;
	public static String rs485_stopbit = null;
	public static String rs485_flow = null;
	public static String rs485_checkbit = null;
	public static String net485_ip = null;
	public static String net485_port = null;
	public static String reboot_485 = null; 
	
	public static String returnBody(String input)
	{
		String body = null;
		String inline = input;
        if(input.equals("GET / HTTP/1.1"))	//为刷新请求，返回目前软件的设置值
        {
        	Log.i("WebString","为刷新请求，返回目前软件的设置值");
        	body = WebString.setString(Rs485Data.ipAddr, 
        			Rs485Data.netMask,Rs485Data.rs485Buad,Rs485Data.rs485bits, Rs485Data.rs485stopBit,Rs485Data.rs485check,Rs485Data.rs485Flow,
        			Rs485Data.net485Ip,Rs485Data.net485port+"","None");
        }
        //提交请求
        else								
        {
        	Log.i("WebString","提交请求");
        	
            Log.i("WS", "ip地址为:" + WebString.getRequestIPaddr("local_ipaddr","&",inline));  
            Log.i("WS", "网关为:" + WebString.getRequestIPaddr("local_netmask","&",inline));  		         
            Log.i("WS", "波特率:" + WebString.getRequestIPaddr("485_buad","&",inline));  		
            Log.i("WS", "数据位:" + WebString.getRequestIPaddr("485_bit","&",inline));  	
            Log.i("WS", "停止位:" + WebString.getRequestIPaddr("485_stopbit","&",inline));  	
            Log.i("WS", "校验位:" + WebString.getRequestIPaddr("485_checkbit","&",inline));  
            Log.i("WS", "流控制:" + WebString.getRequestIPaddr("485_flow","&",inline));  
            Log.i("WS", "串口网络IP:" + WebString.getRequestIPaddr("net485_ip","&",inline));  
            Log.i("WS", "串口网络端口:" + WebString.getRequestIPaddr("net485_port","&",inline));  	
            Log.i("WS", "是否复位转换板:" + WebString.getRequestIPaddr("reboot_485"," HTTP/1.1",inline));  
            
            local_ipaddr = WebString.getRequestIPaddr("local_ipaddr","&",inline);
            local_netmask = WebString.getRequestIPaddr("local_netmask","&",inline);
            rs485_buad = WebString.getRequestIPaddr("485_buad","&",inline);
            rs485_bit = WebString.getRequestIPaddr("485_bit","&",inline);
            rs485_stopbit = WebString.getRequestIPaddr("485_stopbit","&",inline);
            rs485_checkbit = WebString.getRequestIPaddr("485_checkbit","&",inline);
            rs485_flow = WebString.getRequestIPaddr("485_flow","&",inline);
            net485_ip = WebString.getRequestIPaddr("net485_ip","&",inline);
            net485_port = WebString.getRequestIPaddr("net485_port","&",inline);
            reboot_485 = WebString.getRequestIPaddr("reboot_485"," HTTP/1.1",inline);
            
	        //若有一个选择未选择则返回错误提示
			if((local_ipaddr.equals(""))||(local_netmask.equals(""))
				||(rs485_buad.equals(""))||(rs485_bit.equals(""))
				||(rs485_stopbit.equals(""))||(rs485_checkbit.equals(""))||(rs485_flow.equals(""))
			    ||(net485_ip.equals(""))||(net485_port.equals("")))
			{
				Log.i("WebString","若有一个选择未选择则返回错误提示");
				FileHandler file = new FileHandler();
				body = file.readFromFile("/sdcard/2.html");
			}
			else//若全部填入数据
			{
				Log.i("WebString","判断填入数据是否都合法，如果有不合法的，提示错误");
				//判断填入数据是否都合法，如果有不合法的，提示错误
				
				//输入数据都合法，设置数据,将数据应用到APP，用新的界面刷新web
				Rs485Data.ipAddr = local_ipaddr;
				Rs485Data.netMask = local_netmask;
				Rs485Data.rs485Buad = rs485_buad;
				Rs485Data.rs485bits = rs485_bit;
				Rs485Data.rs485stopBit = rs485_stopbit;
				Rs485Data.rs485check = rs485_checkbit;
				Rs485Data.rs485Flow = rs485_flow;
				Rs485Data.net485Ip = net485_ip;
				Rs485Data.net485port = Integer.parseInt(net485_port);
				
				Rs485Data.writeOneLine("ipAddr",Rs485Data.ipAddr);
				Rs485Data.writeOneLine("netMask",Rs485Data.netMask);
				Rs485Data.writeOneLine("rs485Buad",Rs485Data.rs485Buad);
				Rs485Data.writeOneLine("rs485bits",Rs485Data.rs485bits);
				Rs485Data.writeOneLine("rs485stopBit",Rs485Data.rs485stopBit);
				Rs485Data.writeOneLine("rs485check",Rs485Data.rs485check);
				Rs485Data.writeOneLine("rs485Flow",Rs485Data.rs485Flow);
				Rs485Data.writeOneLine("net485Ip",Rs485Data.net485Ip);
//				Rs485Data.writeOneLine("reboot_485",Rs485Data.net485port+"");
				
				JniUart.jniSetUartPara();								/*初始化串口*/
				Jni485.init(Rs485Data.rs485Buad);						/*初始化串口为115200*/
//				Jni485.write("485 OK"); 	
				
				RS485ClientSendThread.closeSocket();					/*当网络断线后，打破485转换口的接收阻塞，使其重新循环连接服务器*/
				 
				body = WebString.setString(local_ipaddr,local_netmask,rs485_buad,rs485_bit,rs485_stopbit,rs485_checkbit,rs485_flow,net485_ip,net485_port,"None");
				
				if(reboot_485.equals("Reboot"))
				{
					Log.e("WebString","产生复位命令");
					Log.e("WebString","复位....");
					String cmd = "reboot";
					RootCmd.execRootCmdSilent(cmd);
				}
			}
			
        }
        if(body == null)
        {
        	Log.e("WebString","web body为空 返回设置错误的页面！！");
			FileHandler file = new FileHandler();
			body = file.readFromFile("/sdcard/2.html");
        }
		return body;
	}
	
	
	
	
}
