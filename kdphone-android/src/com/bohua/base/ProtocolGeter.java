package com.bohua.base;

import java.util.ArrayList;
import android.util.Log;

public class ProtocolGeter {

	protected String xmlfile_path = "";
	
	public static ArrayList<ArrayList<String>> protocol_list;
	public static ArrayList<String> broadcast_action;
	
	public static final String list = "LIST";
	public static final String item = "ITEM";
	
	
	protected void debug(String str) {Log.i("ProtocolGeter",str);}
	
	/*
	 * eg::
		<LIST>
		  <ITEM>APP_NAME:SWITCH_BOARD</ITEM>	应用名：表示这个信息来自于SWITCH_BOARD
		  <ITEM>BC_TYPE:HEALTH</ITEM>			广播类型：表示这个数据的类型为健康信息
		  <ITEM>MSG_TYPE:PORT_STATE</ITEM>		消息类型：PORT_STATE	
		  <ITEM>PORT_ID</ITEM>					消息内容: 交换模块端口ID
		  <ITEM>PORT_STATE</ITEM>				消息内容: 交换模块端口状态
		  <ITEM>TIME</ITEM>						消息内容: 交换模块端口发生变化的时间
		</LIST>
		　　
		<LIST>
		  <ITEM>APP_NAME:SWITCH_BOARD</ITEM>	应用名：表示这个信息来自于SWITCH_BOARD
		  <ITEM>BC_TYPE:HEALTH</ITEM>			广播类型：表示这个数据的类型为健康信息
		  <ITEM>MSG_TYPE:START_RUNNING</ITEM>	消息类型：START_RUNNING	
		  <ITEM>TIME</ITEM>						消息内容: 交换模块开始运行的时间
		</LIST>
     ***/

	
	public void readXMLfile(String string)
	{
		int i = -2;
		int item_num = 0;
		int x = 0;
		Log.i("<AAA> = ", i+"");
//		item_num = readXMLfilelen(string);

		protocol_list = new ArrayList<ArrayList<String>>();
		do 
		{ 
			 Log.i("<DATA_ITEM> = ", "读一条数据");
			 if(i == -2)
			 {
				 i = string.indexOf("<"+list+">");
				 int b = i;
				 Log.i("<DATA_ITEM11> = ", i+"");
				 if(i < 0) break;
				 i = string.indexOf("</"+list+">");
				 Log.i("<DATA_ITEM12> = ", i+"");
				 if(i < 0) break;
				 String temp = string.substring(b+("<"+list+">").length(), i);
				 Log.i("<DATA_ITEM13>  ", "temp = "+temp);
				 x++;
				 readSunList(temp,x);
			 }
			 else
			 {
				 i = string.indexOf("<"+list+">", i);
				 int b = i;
				 Log.i("<DATA_ITEM21> = ", i+"");
				 if(i < 0) break;
				 i = string.indexOf("</"+list+">", i);
				 Log.i("<DATA_ITEM22> = ", i+"");
				 if(i < 0) break;
				 String temp = string.substring(b+("<"+list+">").length(), i);
				 Log.i("<DATA_ITEM13>  ", "temp = "+temp);
				 x++;
				 readSunList(temp,x);
			 }
		}
		while(i != -1);
		Log.i("item_num ", "item_num = "+item_num);
	}
	
	/**返回XML文件中，协议的条数*/
	protected int readXMLfilelen(String string)
	{
		int i = -2;
		int item_num = 0;
		Log.i("<LLL> = ", i+"");
		do 
		{ 
			 Log.i("<DATA_ITEM> = ", "读一条数据");
			 if(i == -2)
			 {
				 i = string.indexOf("<"+list+">");
				 int b = i;
				 Log.i("<DATA_ITEM11> = ", i+"");
				 if(i < 0) break;
				 i = string.indexOf("</"+list+">");
				 Log.i("<DATA_ITEM12> = ", i+"");
				 if(i < 0) break;
				 item_num++;
				 String temp = string.substring(b+("<"+list+">").length(), i);
				 Log.i("<DATA_ITEM13>  ", "temp = "+temp);
			 }
			 else
			 {
				 i = string.indexOf("<"+list+">", i);
				 int b = i;
				 Log.i("<DATA_ITEM21> = ", i+"");
				 if(i < 0) break;
				 i = string.indexOf("</"+list+">", i);
				 Log.i("<DATA_ITEM22> = ", i+"");
				 if(i < 0) break;
				 item_num++;
				 String temp = string.substring(b+("<"+list+">").length(), i);
				 Log.i("<DATA_ITEM13>  ", "temp = "+temp);
			 }
		}
		while(i != -1);
		Log.i("item_num ", "item_num = "+item_num);
		return item_num;
	}
	
	/**读取子列中的各项*/
	protected void readSunList(String string ,int item_line)
	{

		int i = -2;
		ArrayList<String> tempxx = new ArrayList<String>();
		Log.i("<BBB> = ", i+"");
		do 
		{ 
			 Log.i("<DATA_ITEMBB> = ", "读一条数据");
			 if(i == -2)
			 {
				 i = string.indexOf("<"+item+">");
				 int b = i;
				 Log.i("<DATA_ITEM11> = ", i+"");
				 if(i < 0) break;
				 i = string.indexOf("</"+item+">");
				 Log.i("<DATA_ITEM12BB> = ", i+"");
				 if(i < 0) break;
				 String temp = string.substring(b+("<"+item+">").length(), i);
				 Log.i("<DATA_ITEM14BB>  ", "item_line = "+item_line);
				 tempxx.add(temp);
				 Log.i("<DATA_ITEM13BB>  ", "temp = "+temp);
			 }
			 else
			 {
				 i = string.indexOf("<"+item+">", i);
				 int b = i;
				 Log.i("<DATA_ITEM21BB> = ", i+"");
				 if(i < 0) break;
				 i = string.indexOf("</"+item+">", i);
				 Log.i("<DATA_ITEM22BB> = ", i+"");
				 if(i < 0) break;
				 String temp = string.substring(b+("<"+item+">").length(), i);
				 Log.i("<DATA_ITEM14BB>  ", "item_line = "+item_line);
				 tempxx.add(temp);
				 Log.i("<DATA_ITEM13BB>  ", "temp = "+temp);
			 }
		}
		while(i != -1);
		protocol_list.add(tempxx);
	}
	
	/**显示协议表中的所有协议*/
	public void displayarraylist()
	{
		Log.i("displayarraylist ", "tttt len = "+protocol_list.size());
		for(int i=0 ;i < protocol_list.size() ;i++)
		{
			Log.i("displayarraylist ", "子表"+i+"中的参数条目 = "+protocol_list.get(i).size());
			for(int a = 0 ; a<protocol_list.get(i).size() ; a++)
			{
				Log.i("displayarraylist ", "子表"+i+"中的具体数据为 = "+protocol_list.get(i).get(a));
			}
		}
	}
	
	/**获取动态协议表*/
	public ArrayList<ArrayList<String>> getProtocolList()
	{
		return protocol_list;
	}
	
	public int getProtocoListSize()
	{
		return protocol_list.size();
	}
	
	public ArrayList<String> getBroadCastReceiverAction()
	{
		broadcast_action = new ArrayList<String>();
		ArrayList<String> temp = new ArrayList<String>();
			
		if(protocol_list.size() == 1)
		{
			broadcast_action.add(protocol_list.get(0).get(0));
		}
		else if(protocol_list.size() == 0)
		{
		}
		else
		{
			for(int i = 0 ; i < protocol_list.size() ; i++)
			{
				temp.add(protocol_list.get(i).get(0));
			}
			
			for (int i=0; i<temp.size(); i++) {  
	            if(!broadcast_action.contains(temp.get(i))) {  
	            	broadcast_action.add(temp.get(i));  
	            }  
	        }  
		}
		broadcast_action = cutActionString(broadcast_action);
		
		debug("ret = "+broadcast_action.size());
		for(int i = 0 ; i < broadcast_action.size() ; i++)
		{
			debug(broadcast_action.get(i));
		}
		

		
		return broadcast_action;
	}
	
	protected ArrayList<String> cutActionString(ArrayList<String> input)
	{
		ArrayList<String> ret;
		ret = new ArrayList<String>();
		for(int i=0;i<input.size();i++)
		{
			input.get(i).indexOf(':');
			debug("indexOf(':') = "+input.get(i).indexOf(':'));
			String temp = input.get(i).substring(input.get(i).indexOf(':')+1, input.get(i).length());
			debug("temp = "+temp);
			ret.add(temp);
		}
		
		return ret;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
