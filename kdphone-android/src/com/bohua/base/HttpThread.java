package com.bohua.base;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

import android.util.Log;

public class HttpThread extends Thread{
	
	
	private Socket socket;//连接点  
    public HttpThread(Socket socket){//构造方法  
        super();  
        this.socket = socket;  
    }  
   
    //线程方法  
    public void run(){  
        try {  
        	Log.i("HttpThread","HttpThread");
            OutputStream os = socket.getOutputStream();//获得输出流  
            PrintWriter pw = new PrintWriter(os);//创建PrintWriter对象  
            //往输出流写出当前的时间  
            pw.println("<html>");  
            pw.println("<body>");  
            pw.println("hello world!");  
            pw.println("</body>");  
            pw.println("</html>");  
            pw.flush();//清空缓存  
            //关闭输出流和Socket  
            pw.close();  
            socket.close();  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
    }  

}
