package com.bohua.base.thread;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import com.bohua.HDJni.Jni485;
import com.bohua.data.rs485.Rs485Data;

import android.util.Log;

public class RS485ClientSendThread extends Thread{

	public static BufferedReader br = null;
	public static BufferedWriter bw = null;
	public static OutputStream outputStream;
	public static Socket s;
//	private static PrintStream output;
	public String socket_input = null;
	private static int socket_index = -1; 
	
	
	public void run()
	{
		while(true)
		{
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try 
			{
				if(EthernetCheckThread.linkState == false ) return; 
				Log.i("RS485ClientSendThread","RS485SendThread");
				s = new Socket(Rs485Data.net485Ip,Rs485Data.net485port);
//				br = new BufferedReader(new InputStreamReader(s.getInputStream()));
				outputStream = s.getOutputStream();
//				output = new PrintStream(s.getOutputStream(), true, "utf-8");  
				
//				bw.write("------- 485 conver linked socket server OK!!!   -------");
//				bw.flush();  
				
//				byte[] test = new byte[300];
//				for(int i=0;i<256;i++)	test[i] = (byte)i;
				
//				outputStream.write(test);
				
//				bw.write(test);
//				bw.flush();  
				
				socket_index = 0;
//				outPut("------- 485 conver linked socket server OK!!!   -------");  
				int[] ok = new int[10];
				for(int i=0;i<10;i++)	ok[i]=i;
				Jni485.write(ok,1); 
			} 
			catch (IOException e) 
			{ 
				Log.e("RS485ClientSendThread", "服务器连不上！！！");
				//e.printStackTrace();
				socket_index = -1;
			}
			if(socket_index == 0)  
			{ 
				while(true) 
				{
					byte[] buffer = new byte[2999];
					int[] xxxx = new int[2999];
					int len;
					try 
					{
						InputStream inputStream = s.getInputStream();
//						Log.i("RS485ClientSendThread","接收线程");
						if(EthernetCheckThread.linkState == false) 
						{ 
							Log.i("RS485ClientSendThread","网络断开"); 
//							output.close(); 
//							bw.close();
//							br.close();
							s.close(); 
							socket_index = -1;  
							break;
						} 
						if(((len = inputStream.read(buffer)) != -1)) 
						{
							for(int i=0;i<len;i++)
							{
								int m = buffer[i] & 0xFF;  
								xxxx[i] = (int)m;
//								Log.i("RS485ClientSendThread222",Integer.toHexString(m));
							}
							
							Log.i("RS485ClientSendThread","485 send len = "+len);
							Jni485.write(xxxx,len); 
						}
						else 
						{
							Log.i("RS485ClientSendThread","服务器主动关断"); 
//							output.close();
//							bw.close();
//							br.close();
							s.close();
							socket_index = -1;
							break; 
						}
					}
					catch (IOException e) 
					{
						//e.printStackTrace();
						Log.e("RS485ClientSendThread", "网络断开！！！");
						socket_index = -1;
						break;
					}
				}
				Log.i("RS485ClientSendThread","退出数据接收循环");
			}
			Log.i("RS485ClientSendThread","end!!!");
		}
	}
	
	public static boolean outPut(byte[] str)
	{
		boolean state = false;
		if(socket_index == 0)
		{
//			output.print(str);
			try 
			{
				outputStream.write(str);
				state = true;
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
				state = false;
			}
		}
		return state;
	}
	public static void closeSocket()
	{
		if(socket_index == -1) return;
//		output.close();
		
		try 
		{
			bw.close();
			br.close();
			s.close();
		}
		catch (IOException e) 
		{
			Log.e("RS485ClientSendThread", "网络断开！！！主动关闭socket");
		}
		socket_index = -1;
	}
}
