package com.bohua.base.thread;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

import android.util.Log;

public class RS485ServerSendThread extends Thread{

	public BufferedReader br = null;
	public Socket s;
	public String socket_input = null;
	private static int socket_index = -1; 
	private static PrintStream outstream;
	
	public void run(){
		int PORT = 8011;  
		ServerSocket server = null;  
		try {  
		    server = new ServerSocket(PORT);  
		    Log.i("WS", "服务器已启动，监听:" + server.getLocalPort());  
		    
		    for (;;) {  
		        Socket client = server.accept(); // 接受客户机的连接请求  
		  
		        try {  
		            String destIP = client.getInetAddress().toString(); // 客户机IP地址  
		            int destport = client.getPort(); // 客户机端口号  
		            Log.i("WS", "connected to " + destIP + " on port " + destport + ".");  
		  
		            OutputStream out = client.getOutputStream();  
		            outstream = new PrintStream(out);  
		  
		            DataInputStream instream = new DataInputStream(client.getInputStream());  
		            socket_index = 0;
		            while(true)
		            {
		            	byte[] buffer = new byte[3000];
		            	int len;
		            	if(((len = instream.read(buffer)) != -1)) 
		            	{
		            		Log.i("RS485SendThread","len = "+len);
							String strRead = new String(buffer);
							strRead = String.copyValueOf(strRead.toCharArray(), 0, len);
							Log.i("RS485SendThread","buffer = "+strRead);
//							Jni485.write(strRead); 
		            	}
		            }
		        } catch (Exception err) {  
		        	socket_index = -1;
		        	err.printStackTrace();
		            Log.i("WS", err.getMessage());  
		        }  
		    }  
		} catch (Exception err) {  
			err.printStackTrace();
		    Log.i("WS", err.getMessage());  
		}  
	}
	
	
	public static boolean outPut(String str)
	{
		boolean state = false;
		if(socket_index == 0)
		{
			outstream.print(str);
			state = true;
		}
		return state;
	}
	
	
}
