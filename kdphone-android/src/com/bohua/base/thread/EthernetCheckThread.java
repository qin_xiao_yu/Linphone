package com.bohua.base.thread;

import com.bohua.base.Ethernet;
import com.bohua.base.RootCmd;
import com.bohua.data.rs485.Rs485Data;

import android.content.Context;
import android.util.Log;

public class EthernetCheckThread extends Thread{
	
	private Context context;
	public static String Local_ip = null;
	public static boolean linkState;
	private static void Debug(String debug)			{Log.i("EthernetCheckThread",debug);} 
	public EthernetCheckThread(Context context)
	{
		this.context = context;
	}
	public void run()
	{    
		while(true)
		{
			boolean ethernet_index = Ethernet.checkEthernet(context);	/*获取有线的状态*/
			Debug("有线网络状态"+ethernet_index); 
			if(ethernet_index == false)
			{
//				RootCmd.execRootCmdSilent("ifconfig eth0 192.163.20.252 netmask 255.255.255.0");
				Debug("link down~~~~~~~~~  !!-_-!!");
				linkState = false;
				RS485ClientSendThread.closeSocket();					/*当网络断线后，打破485转换口的接收阻塞，使其重新循环连接服务器*/
			}
			else 
			{
				linkState = true;
				Local_ip = Ethernet.getLocalHostIp();
				if(!Local_ip.equals(Rs485Data.ipAddr))
				{
					Debug("改变当前IP"); 
					String cmd = "ifconfig eth0 "+Rs485Data.ipAddr+" netmask "+Rs485Data.netMask;
					RootCmd.execRootCmdSilent(cmd);
//					RootCmd.execRootCmdSilent("ifconfig eth0 192.163.20.252 netmask 255.255.255.0");
				}
			}
			try 
			{
				Thread.sleep(5000);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
		}
		
	}
	
	
}
