package com.bohua.base.thread;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class WDTsendThreader implements Runnable{

	protected static final String tag = "WDTsendThreader";
	protected void debug(String str) {Log.i(tag,str);}
	
	protected static String Action = "SWITCH_BOARD";
	protected Context context;
	public WDTsendThreader(Context context)
	{
		this.context = context;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		debug("WDTsendThreader 开始运行");
		while(true)
		{
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			/*发送WDT广播*/
			Intent intent = new Intent();
	        intent.putExtra("BC_TYPE", "WATCH_DOG");    
	        intent.putExtra("WDT_RSP", "ALIVE");   
	        intent.setAction(Action);   			
	        this.context.sendBroadcast(intent);   				
//	        debug("发送WDT广播");
			
	        //发送健康广播
/*	        intent.putExtra("PROTOCOL_ID", 0);
	        intent.putExtra("BC_TYPE", "HEALTH");    
	        intent.putExtra("MSG_TYPE", "PORT_STATE");   
	        intent.putExtra("PORT_ID", "12");   
	        intent.putExtra("PORT_STATE", "Online");   
	        intent.putExtra("TIME", "2014-8-28 15:26");   
	        intent.setAction(Action);   			
	        this.context.sendBroadcast(intent);   				
	        debug("发送WDT广播");*/
	        
	        intent.putExtra("project_id", "1");
	        intent.putExtra("protocol_id", "14");
	        intent.putExtra("dev_name", "switch_board");
	        intent.putExtra("dev_sn", "1");
	        intent.putExtra("msg_type", "health");
	        intent.putExtra("data_flow", "Te2B");
	        intent.putExtra("port_state", "port1");
	        intent.putExtra("value", "1234");
	        intent.putExtra("time", "2014-8-28 15:26");   
	        intent.setAction(Action);   			
	        this.context.sendBroadcast(intent);   	
			
		}
	}
}
