package com.bohua.base.thread;

import java.io.DataInputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

import com.bohua.base.web.WebString;

import android.util.Log;

public class WebServerSocketThread extends Thread{
	
//	public void run(){
//	    ServerSocket ss;
//		try {
//        	Log.i("serverSocketThread","run");
//			ss = new ServerSocket(4000);
//	        //开始循环监听来自客户端的请求  
//	        Socket s = null;  
//	        while((s = ss.accept()) != null){  
//	            new HTTPThread(s).start();//开始一个新的线程  
//	        }  
//	        ss.close();  
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}  	
//	}
	public void run(){
		int PORT = 8010;  
		ServerSocket server = null;  
		try 
		{  
		    server = new ServerSocket(PORT);  
		    Log.i("WS", "服务器已启动，监听:" + server.getLocalPort());  
		    for (;;) 
		    {  
		        Socket client = server.accept(); // 接受客户机的连接请求  
		  
		        try 
		        {  
		            String destIP = client.getInetAddress().toString(); // 客户机IP地址  
		            int destport = client.getPort(); // 客户机端口号  
		            Log.i("WS", "connected to " + destIP + " on port " + destport + ".");  
		  
		            OutputStream out = client.getOutputStream();  
		            PrintStream outstream = new PrintStream(out);  
		  
		            DataInputStream instream = new DataInputStream(client.getInputStream());  
		            
		            @SuppressWarnings("deprecation")
					String inline = instream.readLine(); //读取Web浏览器提交的请求信息  
		            Log.i("WS", "request:" + inline);  
		            
		            outstream.println("HTTP/1.1 200 OK");//返回的状态，必须加入，否则浏览器显示无法加载  
		            outstream.println("Content-Type:text/html;");//返回的类型  
		            outstream.println();//响应头与主体内容之间必需空一行，否则浏览器无法读取内容  

		            String body = WebString.returnBody(inline);
		            
		            outstream.println(body);  
		            outstream.flush();  
		  
		            client.shutdownInput();//必需关闭流，否则浏览器将无法打开  
		            client.shutdownOutput();  
		            client.close();  
		        } 
		        catch (Exception err) 
		        {  
		        	Log.e("WS", "刷新web页面有问题");  
		        	err.printStackTrace();
		        }  
		    }  
		} 
		catch (Exception err) 
		{  
			Log.e("WS", "创建web socket错误");  
			err.printStackTrace();
		}  
	}
	
	

}
