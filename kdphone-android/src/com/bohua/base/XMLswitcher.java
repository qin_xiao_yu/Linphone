package com.bohua.base;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.Socket;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;


/**这是DOM解析XML文件通用性极高的类，应为一般应用的很多所以将其方法放入静态区
 * 其功能是：1.将XML文件转换为Document对象
 *          2.将Document转换为XML文件
 *          3.生成一个Document对象*/

public class XMLswitcher{
	
	/**把path路径所代表的XML文件转换为Document对象*/
	public static org.w3c.dom.Document loadXMLfile(String path) {
		org.w3c.dom.Document document = null;
		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder builder = documentBuilderFactory
					.newDocumentBuilder();
			//parse()方法解析一个xml文件并返回一个document对象
			document = builder.parse(new File(path)); 
			document.normalize();//规整化xml文件，删除空的Text节点
			return document;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
	
	/**将Document对象储存为path路径所代表的文件*/
	public  static boolean saveXML(org.w3c.dom.Document document, String path) {
		try {
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();

			DOMSource source = new DOMSource(document);
			StreamResult streamResult = new StreamResult(new FileOutputStream(path));
			transformer.transform(source, streamResult);//看了半天这些API好像就是为了XML的一套处理方式
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;	
		}
	}
	
	/**凭空生成一个Document对象，有了这个方法这个类的名字就不太合适了，以后再想*/
	public static org.w3c.dom.Document initDocument(){
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder = null;
		try {
			documentBuilder = documentBuilderFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			return null;
		}
		org.w3c.dom.Document document = documentBuilder.newDocument();
		return document;
	}
	
	/**发送一个XML对象对应的文件区socket*/
	public  static boolean sendXMLToSocket(org.w3c.dom.Document document, Socket socket) {
		try {
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();

			DOMSource source = new DOMSource(document);
			OutputStream outputStream = socket.getOutputStream();
			StreamResult streamResult = new StreamResult(outputStream);
			transformer.transform(source, streamResult);//看了半天这些API好像就是为了XML的一套处理方式
			outputStream.flush();
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;	
		}
	}
	/**从一个输入流中读除一个document对象 ，这两个方法与前两个方法的唯一区别就是将文件流改成了socket中的流*/
	public static org.w3c.dom.Document ReceiveFromSocket(Socket socket) {
		org.w3c.dom.Document document = null;
		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder builder = documentBuilderFactory
					.newDocumentBuilder();
			//parse()方法解析一个xml文件并返回一个document对象
			
			document = builder.parse(socket.getInputStream()); 
			document.normalize();//规整化xml文件，删除空的Text节点
			return document;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
}