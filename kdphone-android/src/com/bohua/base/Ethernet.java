package com.bohua.base;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import org.apache.http.conn.util.InetAddressUtils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Ethernet {
	
	private static void Debug(String debug)			{/*Log.i("Ethernet",debug);*/} 
	
	public static boolean checkEthernet(Context context)
	{
		ConnectivityManager conn =(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = conn.getNetworkInfo(ConnectivityManager.TYPE_ETHERNET);
		return networkInfo.isConnected();
	}
	
	
	public static String getLocalHostIp() 
	{
		String ipaddress = "";

		try 
		{
			Enumeration<NetworkInterface> en = NetworkInterface
			.getNetworkInterfaces();
			// Traverse the network interface
			while (en.hasMoreElements()) 
			{
				NetworkInterface nif = en.nextElement();// Each network
				// interface binding,all
				// IP
				Enumeration<InetAddress> inet = nif.getInetAddresses();
				// Traverse each all the IP interface binding
				while (inet.hasMoreElements()) 
				{
					InetAddress ip = inet.nextElement();
					if (!ip.isLoopbackAddress()&& InetAddressUtils.isIPv4Address(ip.getHostAddress())) 
					{
						Debug(ip.getHostAddress());
						return ipaddress = ip.getHostAddress();
					}
				}
			}
		} 
		catch (SocketException e) 
		{
			Debug("��ȡ����IPʧ��");
			e.printStackTrace();
		}
		return ipaddress;
		}
}