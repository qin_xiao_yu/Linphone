package com.bohua.base;



import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.NodeList;

import android.content.Context;
import android.util.Log;

/**BaseXMLWorker唯一服务于公司通用XML文件。该文件用于存放公司维护数据的协议
 * 该文件暂时被定义成
 *<root>
 *	<AAA>
 *		<BBB>healthLog:main</BBB>
 *		<BBB>name:string></BBB>
 *		<BBB>id:int</BBB>
 *	</AAA>
 * 	<AAA>
 *		<BBB>runtimeLog:main</BBB>
 *		<BBB>name:string></BBB>
 *		<BBB>id:int</BBB>
 *	</AAA>
 *</root>                    
 *
 *   由于要曾强通用性，所以基本原则是写死的标签没有实际意义,而其中的数据就是其他XML文件的标签
 *   功能：
 *   	1核心功能，读取该XML文件并把标签解析成字符串数组
 *   	2这个文件的获取途径：一种是从开始的APK包里把这个文件拷贝到相应的目录下，
 *   		另一种是自动生成这种XML文件，用于测试*/

public class BaseXMLWorker{
	
	
	@SuppressWarnings("unused")
	private Context context;
	private  static  final String tag = "BaseXMLWorker";
	
	/**构造器，用于保存context*/
	public BaseXMLWorker(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
	}
	
	public List<List<String>> getItemFromXML(String path)
	{
		List<List<String>> aitems = new ArrayList<List<String>>();
		org.w3c.dom.Document document = XMLswitcher.loadXMLfile(path);
		NodeList AAAnodeList = document.getElementsByTagName("AAA");
		Log.i(tag,"AAAnodeList.getLength() = "+AAAnodeList.getLength());
		
		
		NodeList BBBnodeList = AAAnodeList.item(0).getChildNodes();
		Log.i(tag,"BBBnodeList.getLength() = "+BBBnodeList.getLength());
		
		
//		for(int i = 0 ; i < AAAnodeList.getLength() ; i++)
//		{
//			List<String> bitems = new ArrayList<String>();
//			NodeList BBBnodeList = AAAnodeList.item(i).getChildNodes();
//			for(int j = 0 ; j < BBBnodeList.getLength() ; j ++)
//			{
//				Log.i(tag,"BBBnodeList.getLength() = "+BBBnodeList.getLength());
//				String item = BBBnodeList.item(j).getFirstChild().getNodeValue();
//				Log.i(tag,"item = "+item);
//				bitems.add(item);
//			}
//			aitems.add(bitems);
//		}
		return aitems;
	}
	
	/**该方法用于生成一个XML文件用于测试
	 * @注释:应为只是测试用的代码，所以没有另外做复用模块*/
//	public void initBaseXML(){
//		String runtimeHealthlog[] = context.getApplicationContext().
//				getResources().getStringArray(R.array.runtime_healthlog);
//		String batteryHealthlog[] = context.getApplicationContext().
//				getResources().getStringArray(R.array.battery_healthlog);
//		String wifiHealthlog[] = context.getApplicationContext().
//				getResources().getStringArray(R.array.wifi_healthlog);
//		String appHealthlog[] = context.getApplicationContext().
//				getResources().getStringArray(R.array.app_healthlog);
//		org.w3c.dom.Document document = XMLswitcher.initDocument();
//		
//		//创建根节点
//		org.w3c.dom.Element root = document.createElement("root");
//		document.appendChild(root);
//		
//		//创建AAA节点
//		org.w3c.dom.Element AAA1 = document.createElement("AAA");
//		root.appendChild(AAA1);
//		for(int i = 0 ; i < runtimeHealthlog.length ; i++){
//			org.w3c.dom.Element element = document.createElement("BBB");
//			org.w3c.dom.Text text = document.createTextNode(runtimeHealthlog[i]);
//			element.appendChild(text);
//			AAA1.appendChild(element);
//		}
//		org.w3c.dom.Element AAA2 = document.createElement("AAA");
//		root.appendChild(AAA2);
//		for(int i = 0 ; i < batteryHealthlog.length ; i++){
//			org.w3c.dom.Element element = document.createElement("BBB");
//			org.w3c.dom.Text text = document.createTextNode(batteryHealthlog[i]);
//			element.appendChild(text);
//			AAA2.appendChild(element);
//		}
//		org.w3c.dom.Element AAA3 = document.createElement("AAA");
//		root.appendChild(AAA3);
//		for(int i = 0 ; i < wifiHealthlog.length ; i++){
//			org.w3c.dom.Element element = document.createElement("BBB");
//			org.w3c.dom.Text text = document.createTextNode(wifiHealthlog[i]);
//			element.appendChild(text);
//			AAA3.appendChild(element);
//		}
//		org.w3c.dom.Element AAA4 = document.createElement("AAA");
//		root.appendChild(AAA4);
//		for(int i = 0 ; i < appHealthlog.length ; i++){
//			org.w3c.dom.Element element = document.createElement("BBB");
//			org.w3c.dom.Text text = document.createTextNode(appHealthlog[i]);
//			element.appendChild(text);
//			AAA4.appendChild(element);
//		}
//		File sd = Environment.getExternalStorageDirectory();
//		String path = sd.getPath()+"/baseXML.xml";
//		File file = new File(path);
//		if(file.exists()){
//			file.delete();
//		}
//		XMLswitcher.saveXML(document, path);
//		
//	}
}