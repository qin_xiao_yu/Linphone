//package com.bohua.switchboard;
//
//import java.util.List;
//
//import com.bohua.switchboard.R;
//import com.bohua.HDJni.Jni485;
//import com.bohua.HDJni.JniUart;
//import com.bohua.base.BroadCastReceiver;
//import com.bohua.base.FileHandler;
//import com.bohua.base.ProtocolGeter;
//import com.bohua.base.RootCmd;
//import com.bohua.base.thread.EthernetCheckThread;
//import com.bohua.base.thread.RS485ClientReciveThread;
//import com.bohua.base.thread.RS485ClientSendThread;
//import com.bohua.base.thread.RS485ReadThread;
//import com.bohua.base.thread.UartReciveThread;
//import com.bohua.base.thread.WebServerSocketThread;
//import com.bohua.data.rs485.Rs485Data;
//
//import android.app.Activity;
//import android.content.BroadcastReceiver;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.os.Bundle;
//import static com.bohua.base.Debuger.*;
//
//
//public class SwitchBoard extends Activity {
//	/*****广播*****/ 
//	@SuppressWarnings("unused")
//	private BroadCastReceiver broadCastReceiver;				//广播实例
//	protected final String rcvBoardCastAction = "defalut";		//接收广播监听关键字（未修改）
//	protected final String sendBoardCastAction = "default";		//发送广播监听关键字（未修改）
//	protected BroadcastReceiver broadcastReceiver;
//	
//	
//	@SuppressWarnings("unused")
//	private List<List<String>> itemss;
//	 
//	@Override
//	protected void onCreate(Bundle savedInstanceState) { 
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_main);
//		MainDebug("初始化：：onCreate");
//		 
//		Rs485Data.loadDefaultParam();
//		Rs485Data.readAllConfigData();
//		 
////		establishProtocolList();								/*建立协议表*/
////		registerBoardCast();									/*注册广播接收器*/
//		delay(500);
//		RootCmd.execRootCmdSilent("insmod /sdcard/switch.ko");	/*加载485需要的模块*/
//
//		JniUart.jniSetUartPara();								/*初始化串口*/
//		Jni485.init(Rs485Data.rs485Buad);						/*初始化串口为115200*/
//		
//		delay(500);
//		new Thread(new EthernetCheckThread(getApplicationContext())).start();   /*启动有线网络检测线程，动态改变本地IP*/
//		new Thread(new UartReciveThread()).start();								/*打开串口接收子线程*/
////		new Thread(new WDTsendThreader(getApplicationContext())).start();		/*启动APP watch dog线程*/
//		delay(500);
//		new Thread(new RS485ClientSendThread()).start();  						/*启动485客户端发送线程，启动网络连接，使能485发送功能*/
//		delay(500);
//		new Thread(new RS485ReadThread()).start(); 
//		delay(500);
//		new Thread(new RS485ClientReciveThread()).start();   					/*启动485客户端接收线程，启动485口数据接收*/
////		new Thread(new RS485ServerSendThread()).start();  						/*启动485服务器端发送线程，启动网络连接，使能485发送功能*/
////		delay(500);
////		new Thread(new RS485ServerReciveThread()).start();   					/*启动485服务器端接收线程，启动485口数据接收*/
//		new Thread(new WebServerSocketThread()).start();  						/*启动Web服务器线程*/
//
//  
//		
//
//		MainDebug("初始化：：onCreate over!!"); 
//
//		
//	}
//	private void delay(int time)
//	{
//		try {
//			Thread.sleep(time);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
//	 
//
//	
//	/*建立协议表*/
//	protected void establishProtocolList()
//	{
//		ProtocolGeter xml_reader = new ProtocolGeter();
//		FileHandler fileHandler = new FileHandler();
//		String file_str = fileHandler.readFromFile("/baseXML.xml");
//		MainDebug("/sdcard/baseXML.xml = "+file_str);
//		xml_reader.readXMLfile(file_str);
//		xml_reader.displayarraylist();
//		xml_reader.getBroadCastReceiverAction();
//	}
//	 
//	/*注册广播接收器*/
//	protected void registerBoardCast()
//	{
//		MainDebug("初始化：：注册全局广播");
////		ArrayList<BroadCastReceiver> broadCastReceiverList;
////		broadCastReceiverList = new ArrayList<BroadCastReceiver>();
////		for(int i=0;i<ProtocolGeter.broadcast_action.size();i++)
////		{
////			broadCastReceiverList.add(new BroadCastReceiver());
////			IntentFilter filter = new IntentFilter(); 
////			filter.addAction(ProtocolGeter.broadcast_action.get(i));  	/*只有持有相同的action的接受者才能接收此广播*/
////			registerReceiver(broadCastReceiverList.get(i), filter);
////		}
//		
//		BroadCastReceiver broadCastRecevier = new BroadCastReceiver();
//		IntentFilter filter = new IntentFilter(); 
//		for(int i=0;i<ProtocolGeter.broadcast_action.size();i++)
//			filter.addAction(ProtocolGeter.broadcast_action.get(i));
//		registerReceiver(broadCastRecevier, filter);
//	}
//  
//	protected void sendBoardCast(String msg)
//	{
//		Intent intent = new Intent();
//        intent.putExtra("data", msg);   
//        intent.setAction(sendBoardCastAction);   			
//        sendBroadcast(intent);   										/*发送广播*/
//	}
// 
//}
